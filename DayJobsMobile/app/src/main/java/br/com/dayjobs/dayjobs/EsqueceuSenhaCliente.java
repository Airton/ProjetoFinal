package br.com.dayjobs.dayjobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by P on 18/04/2017.
 */

public class EsqueceuSenhaCliente extends AppCompatActivity {
    private static String METHOD_NAME = "";
    SoapPrimitive result;
    String resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.esqueceu_senha_cliente);
    }

    //Botão BACK padrão do android
    @Override
    public void onBackPressed() {
        //O efeito ao ser pressionado do botão (no caso abre a activity)
        startActivity(new Intent(this, LoginClienteActivity.class));
        //Método para matar a activity e não deixa-lá indexada na pilhagem
        finish();
        return;
    }

    public void EnviarEmail(View v) {
        Thread nt = new Thread() {
            EditText txtEmail = (EditText) findViewById(R.id.txtEmail);

            @Override
            public void run() {
                String SOAP_ACTION = "http://webservice.dayjobs.com.br/EsqueceuSenhaCliente";
                String NAMESPACE = "http://webservice.dayjobs.com.br/";
                METHOD_NAME = "EsqueceuSenhaCliente";
                String URL = "http://webservice.dayjobs.com.br/WebService.asmx?WSDL";

                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                //requisição passando o parametro do metodo e o valor a colocar.
                request.addProperty("email", txtEmail.getText().toString());

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;

                envelope.setOutputSoapObject(request);

                HttpTransportSE transporte = new HttpTransportSE(URL);

                try {
                    transporte.call(SOAP_ACTION, envelope);
                    result = (SoapPrimitive) envelope.getResponse();
                    resultado = result.getValue().toString();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (resultado.equals("true")) {
                            Intent intent = new Intent(EsqueceuSenhaCliente.this, LoginClienteActivity.class);
                            intent.putExtra("retorno", "true");
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(EsqueceuSenhaCliente.this, "O email não está cadastrado no sistema!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        };
        nt.start();
    }
}