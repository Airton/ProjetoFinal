package br.com.dayjobs.dayjobs.ClasseBasica;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by P on 28/04/2017.
 */

    public class Servicos implements Parcelable{
    public Servicos(){

    }

    public Servicos(Parcel in){

    }

    public Servicos(String teste) {
        ID_SERVICO = 4;
        NOME_SERVICO = "Faxina";
        DESCRICAO_SERVICO = "Limpeza em geral";

    }

    public static final Creator<Servicos> CREATOR = new Creator<Servicos>() {
        @Override
        public Servicos createFromParcel(Parcel in) {
            return new Servicos(in);
        }

        @Override
        public Servicos[] newArray(int size) {
            return new Servicos[size];
        }
    };

    private int ID_SERVICO;
    private String NOME_SERVICO;
    private String DESCRICAO_SERVICO;
    private boolean CHECK;


    public int getID_SERVICO() {
        return ID_SERVICO;
    }

    public void setID_SERVICO(int ID_SERVICO) {
        this.ID_SERVICO = ID_SERVICO;
    }

    public String getNOME_SERVICO() {
        return NOME_SERVICO;
    }

    public void setNOME_SERVICO(String NOME_SERVICO) {
        this.NOME_SERVICO = NOME_SERVICO;
    }

    public String getDESCRICAO_SERVICO() {
        return DESCRICAO_SERVICO;
    }

    public void setDESCRICAO_SERVICO(String DESCRICAO_SERVICO) {
        this.DESCRICAO_SERVICO = DESCRICAO_SERVICO;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID_SERVICO);
        dest.writeString(NOME_SERVICO);
        dest.writeString(DESCRICAO_SERVICO);

    }

    public boolean isCHECK() {
        return CHECK;
    }

    public void setCHECK(boolean CHECK) {
        this.CHECK = CHECK;
    }
}
