package br.com.dayjobs.dayjobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import br.com.dayjobs.dayjobs.ClasseBasica.Diarista;

public class TelaPrincipalDiarista extends livroandroid.lib.activity.BaseActivity {

    private static String METHOD_NAME = "";
    SoapPrimitive result;
    String retorno;

    protected DrawerLayout drawerLayout;
    Diarista diarista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_principal_diarista);

        Intent it = getIntent();
        diarista = it.getParcelableExtra("diarista");

        Toast.makeText(this, "Seja bem-vinda, " + diarista.getNOME_DIARISTA() + "!", Toast.LENGTH_LONG).show();

        Switch onOffSwitch = (Switch) findViewById(R.id.switch1);
        onOffSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.v("Switch State = ", "" + isChecked);
                if (isChecked) {
                    alterarStatus("ONLINE");
                } else {
                    alterarStatus("OFFLINE");
                }
            }
        });

        setUpToolbar();
        setupNavDrawer();

        MapsFragment mapsFragment = new MapsFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.fragment_container_diarista, mapsFragment, "MapsFragment");
        transaction.commitAllowingStateLoss();
    }

    public void alterarStatus(String status) {
        if (status == "ONLINE") {
            Thread nt = new Thread() {
                @Override
                public void run() {
                    String SOAP_ACTION = "http://webservice.dayjobs.com.br/AlterarStatusDiarista";
                    String NAMESPACE = "http://webservice.dayjobs.com.br/";
                    METHOD_NAME = "AlterarStatusDiarista";
                    String URL = "http://webservice.dayjobs.com.br/WebService.asmx?WSDL";

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    // requisição passando o parametro do metodo e o valor a colocar.
                    request.addProperty("status", "ONLINE");
                    request.addProperty("id", diarista.getID_DIARISTA());

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet = true;

                    envelope.setOutputSoapObject(request);

                    HttpTransportSE transporte = new HttpTransportSE(URL);

                    try {
                        transporte.call(SOAP_ACTION, envelope);
                        result = (SoapPrimitive) envelope.getResponse();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(TelaPrincipalDiarista.this, "Você está ONLINE", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            };
            nt.start();
        } else {
            Thread nt = new Thread() {
                @Override
                public void run() {
                    String SOAP_ACTION = "http://webservice.dayjobs.com.br/AlterarStatusDiarista";
                    String NAMESPACE = "http://webservice.dayjobs.com.br/";
                    METHOD_NAME = "AlterarStatusDiarista";
                    String URL = "http://webservice.dayjobs.com.br/WebService.asmx?WSDL";

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    // requisição passando o parametro do metodo e o valor a colocar.
                    request.addProperty("status", "OFFLINE");
                    request.addProperty("id", diarista.getID_DIARISTA());

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet = true;

                    envelope.setOutputSoapObject(request);

                    HttpTransportSE transporte = new HttpTransportSE(URL);

                    try {
                        transporte.call(SOAP_ACTION, envelope);
                        result = (SoapPrimitive) envelope.getResponse();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(TelaPrincipalDiarista.this, "Você está OFFLINE", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            };
            nt.start();
        }
    }

    //Botão BACK padrão do android
    @Override
    public void onBackPressed() {
        //O efeito ao ser pressionado do botão (no caso abre a activity)
        startActivity(new Intent(this, LoginClienteActivity.class));
        //Método para matar a activity e não deixa-lá indexada na pilhagem
        finish();
        return;
    }

    //Métodos da BaseActivity -> Início
    private FragmentManager baseFragmentManager;

    //Configura a Toolbar
    protected void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_diarista);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    //Configura o nav drawer
    protected void setupNavDrawer() {
        //Drawer Layout
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            //Ícone do menu do Nav Drawer
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setDisplayHomeAsUpEnabled(true);
            drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_diarista);
            if (navigationView != null && drawerLayout != null) {
                //Atualizar a imagem e os textos do header do menu lateral
                setNavViewValues(navigationView, diarista.getNOME_DIARISTA(), diarista.getEMAIL_DIARISTA(), diarista.getFOTO_DIARISTA());
                //Trata o evento de clique no menu
                navigationView.setNavigationItemSelectedListener(
                        new NavigationView.OnNavigationItemSelectedListener() {
                            @Override
                            public boolean onNavigationItemSelected(MenuItem menuItem) {
                                // Seleciona a linha
                                menuItem.setChecked(true);
                                // Fecha o menu
                                drawerLayout.closeDrawers();
                                // Trata o evento do menu
                                onNavDrawerItemSelected(menuItem);
                                return true;
                            }
                        });
            }
        }
    }

    //Atualiza os dados do header do Navigation View
    private void setNavViewValues(NavigationView navView, String nome, String email, String foto) {
        View headerView = navView.getHeaderView(0);
        TextView tNome = (TextView) headerView.findViewById(R.id.nome_perfil);
        TextView tEmail = (TextView) headerView.findViewById(R.id.email_perfil);
        ImageView imgView = (ImageView) headerView.findViewById(R.id.img_perfil);
        tNome.setText(nome);
        tEmail.setText(email);
        Glide.with(this).load(foto).into(imgView);
    }

    //Trata o evento do menu lateral
    private void onNavDrawerItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_item_principal:
                TelaPrincipalDiarista.this.onStart();
                break;
            case R.id.nav_item_perfil:
                Intent intent1 = new Intent(this, PerfilDiaristaActivity.class);
                intent1.putExtra("diarista", diarista);
                startActivity(intent1);
                break;
            case R.id.nav_item_servico:
                Intent intent2 = new Intent(this, ServicosDiarista.class);
                intent2.putExtra("diarista", diarista);
                startActivity(intent2);
                break;
            case R.id.nav_item_sair:
                alterarStatus("OFFLINE");
                Intent intent3 = new Intent(getApplicationContext(), LoginDiaristaActivity.class);
                startActivity(intent3);
                getActivity().finish();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Trata o clique no botão que abre o menu
                if (drawerLayout != null) {
                    openDrawer();
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    // Abre o menu lateral
    protected void openDrawer() {
        if (drawerLayout != null) {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    // Fecha o menu lateral
    protected void closeDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }
    //Métodos da BaseActivity -> Fim

}