package br.com.dayjobs.dayjobs;

import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.dayjobs.dayjobs.ClasseBasica.Diarista;

public class TelaSelecionarDiarista extends AppCompatActivity {

    private GoogleMap mMap;
    LatLng mOrigemDiarista, mOrigemCliente, mOrigem;
    private LocationManager locationManager;
    Diarista lista, diarista, objeto;
    BitmapDescriptor icon;
    List<Diarista> listaDiarista;
    SoapObject result, resultDiarista;
    private static String METHOD_NAME = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_selecionar_diarista);

        Intent i = new Intent();
        i.putExtra("msg", "1º Opção:");
        setResult(1, i);
    }

}