package br.com.dayjobs.dayjobs.ClasseBasica;

/**
 * Created by Carlos Eduardo on 24/04/2017.
 */

public class TipoLogin {
    public TipoLogin() {

    }

    public TipoLogin(String retorno1, String retorno2) {
        TipoLogin tipoLogin = new TipoLogin();
        if (retorno1 != null) {
            tipoLogin.setLoginDiarista("Diarista");
        }
        if (retorno2 != null) {
            tipoLogin.setLoginCliente("Usuário");
        }
    }

    private static String LoginDiarista;
    private static String LoginCliente;

    public static String getLoginDiarista() {
        return LoginDiarista;
    }

    public static void setLoginDiarista(String loginDiarista) {
        LoginDiarista = loginDiarista;
    }

    public static String getLoginCliente() {
        return LoginCliente;
    }

    public static void setLoginCliente(String loginCliente) {
        LoginCliente = loginCliente;
    }

}
