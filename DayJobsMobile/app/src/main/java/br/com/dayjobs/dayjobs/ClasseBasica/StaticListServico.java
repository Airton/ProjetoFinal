package br.com.dayjobs.dayjobs.ClasseBasica;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Carlos Eduardo on 27/05/2017.
 */

public class StaticListServico {
    Servicos item;
    public StaticListServico(){
    }
    public StaticListServico(List<Servicos> listasSelecionada){
            if (ListasSelecionada == null){
                ListasSelecionada = new ArrayList<>();
            }
            ListasSelecionada = listasSelecionada;

    }
   private static List<Servicos> ListasSelecionada;

    public static List<Servicos> getListasSelecionada() {
        return ListasSelecionada;
    }

    public static void setListasSelecionada(List<Servicos> listasSelecionada) {
        ListasSelecionada = listasSelecionada;
    }
}
