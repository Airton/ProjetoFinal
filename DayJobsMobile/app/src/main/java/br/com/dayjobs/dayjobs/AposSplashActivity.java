package br.com.dayjobs.dayjobs;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Brendo Santiago on 21/05/2017.
 */

public class AposSplashActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnLogar;
    Button btnCadastrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide(); //Mágicaaa
        setContentView(R.layout.activity_apos_splash);

        btnLogar = (Button) findViewById(R.id.btn_logar);
        btnLogar.setOnClickListener(this);

        btnCadastrar = (Button) findViewById(R.id.btn_cadastrar);
        btnCadastrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == findViewById(R.id.btn_logar).getId()) {
            Intent logar = new Intent(this, EscolhaLoginActivity.class);
            startActivity(logar);
        } else {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.dayjobs.com.br/EscolhaLogin.html"));
            startActivity(intent);
        }
    }
}
