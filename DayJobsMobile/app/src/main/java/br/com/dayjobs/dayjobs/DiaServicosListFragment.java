package br.com.dayjobs.dayjobs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.dayjobs.dayjobs.Adapter.DiaristaAdapter;
import br.com.dayjobs.dayjobs.ClasseBasica.Cliente;
import br.com.dayjobs.dayjobs.ClasseBasica.Diarista;
import br.com.dayjobs.dayjobs.ClasseBasica.Proposta;

/**
 * Created by Carlos Eduardo on 08/05/2017.
 */

public class DiaServicosListFragment extends Fragment{
    List<String> mDiarista;
    ArrayAdapter<String> mAdapter;
    TextView txtnome;
    TextView txtemail;
    RatingBar ratestrelas;
    Diarista diarista;
    Cliente cliente;
    ListView ListaDeServicos;
    Proposta proposta;
    String tipoLogin;
    public static final String EXTRA_DETALHE = "tagDetalhe";
    public static final String EXTRA_DIARISTA = "diarista";

    Diarista objeto;
    Cliente objetoCliente;
    Diarista objeto2;
    Cliente objetoCliente2;
    SoapObject result;
    String retorno;
    SoapObject resultsRequestSOAP;
    List<Proposta> listaTest;
    SoapObject soapObjectProposta;
    private static String METHOD_NAME = "";
    public DiaServicosListFragment(){

    }


    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        diarista = (Diarista) getArguments().getSerializable(EXTRA_DIARISTA);
        //setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(
                R.layout.frame_lista_proposta,container,false);

            diarista = new Diarista();
            proposta = new Proposta("teste");

        ListaDeServicos = (ListView) layout.findViewById(R.id.Lista_Proposta);
        List<Proposta> listTest;
        listTest = new ArrayList<Proposta>();
        listTest.add(proposta);
        listTest.add(proposta);
        listTest.add(proposta);

        if(listTest != null){
            //ListaDeServicos.setAdapter(new DiaristaAdapter(getActivity(), listaTest));
              ListaDeServicos.setAdapter(new DiaristaAdapter(getActivity(), listTest));
        }

        return layout;
    }

    private List<String> carregarServicos(){
        List<String> diaristas = new ArrayList<String>();
        Diarista obj = new Diarista();
        String nome = obj.getNOME_DIARISTA();
        String email = obj.getEMAIL_DIARISTA();
        diaristas.add(nome);
        diaristas.add(email);

       // diaristas.add(new Diarista(obj.getNOME_DIARISTA()));
        return diaristas;
    }

    private void carregarObjetoDiarista(){
        List<String> diaristas = new ArrayList<String>();
        Diarista obj = new Diarista();
        txtnome.setText(obj.getNOME_DIARISTA().toString());
        txtemail.setText(obj.getEMAIL_DIARISTA().toString());

    }

    public void thread(){

            Thread nt = new Thread() {
                int res;
                int res2;
                @Override
                public void run() {

                    METHOD_NAME = "ListarProposta";

                    String SOAP_ACTION2 = "http://webservice.dayjobs.com.br/ListarProposta";

                    String NAMESPACE = "http://webservice.dayjobs.com.br/";


                    String URL = "http://webservice.dayjobs.com.br/WebService.asmx?WSDL";
                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    //request.addProperty("Tipo_Login", tipoLogin);

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet=true;

                    envelope.setOutputSoapObject(request);

                    HttpTransportSE transporte = new HttpTransportSE(URL);

                    try {
                            transporte.call(SOAP_ACTION2, envelope);

                        {

                        }

                        resultsRequestSOAP = (SoapObject) envelope.getResponse();
                        listaTest = new ArrayList<>();
                        for(int cont=0 ; resultsRequestSOAP.getPropertyCount() > cont; cont++) {
                            soapObjectProposta = (SoapObject) resultsRequestSOAP.getProperty(cont);
                            proposta = new Proposta();
                            ObjPropostaPreencher();
                            listaTest.add(proposta);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    }
                }
            };
            nt.start();
        }
    public void ObjPropostaPreencher(){
        try {
            if(soapObjectProposta.hasProperty("IdProposta")){
                proposta.setID_PROPOSTA(Integer.parseInt(soapObjectProposta.getProperty("IdProposta").toString()));
            }
            proposta.setID_DIARISTA(Integer.parseInt(soapObjectProposta.getProperty("IdDiarista").toString()));
            proposta.setID_CLIENTE(Integer.parseInt(soapObjectProposta.getProperty("IdCliente").toString()));
            proposta.setAVALIACAO_PROPOSTA(soapObjectProposta.getPropertySafelyAsString("AvaliacaoProposta").toString());
           // proposta.setHORARIOFIM_PROPOSTA(soapObjectProposta.getPropertySafelyAsString("HorarioInicioProposta").toString());
            proposta.setSITUACAO_PROPOSTA(soapObjectProposta.getPropertySafelyAsString("SituacaoProposta").toString());
            proposta.setCEP_PROPOSTA(soapObjectProposta.getPropertySafelyAsString("CepProposta").toString());
            proposta.setLOGRADOURO_PROPOSTA(soapObjectProposta.getPropertySafelyAsString("LogradouroProposta").toString());
            proposta.setNUMERO_PROPOSTA(soapObjectProposta.getPropertySafelyAsString("NumeroProposta").toString());
            proposta.setCOMPLEMENTO_PROPOSTA(soapObjectProposta.getPropertySafelyAsString("ComplementoProposta").toString());
            proposta.setBAIRRO_PROPOSTA(soapObjectProposta.getPropertySafelyAsString("BairroProposta").toString());
            proposta.setCIDADE_PROSPOSTA(soapObjectProposta.getPropertySafelyAsString("CidadeProposta").toString());
            proposta.setUF_PROPOSTA(soapObjectProposta.getPropertySafelyAsString("UfProposta").toString());
            proposta.setLATITUDE_PROPOSTA(soapObjectProposta.getPropertySafelyAsString("LatitudeProposta").toString());
            proposta.setLONGITUDE_PROPOTA(soapObjectProposta.getPropertySafelyAsString("LongitudeProposta").toString());
            proposta.setCLASSIFICACAO_PROPOSTA(Double.parseDouble(soapObjectProposta.getPropertySafelyAsString("ClassificacaoProposta").toString()) );
            proposta.setAVALIACAO_PROPOSTA(soapObjectProposta.getPropertySafelyAsString("AvaliacaoProposta").toString());
        }
        catch (Exception ex){
            Toast.makeText(getActivity(), "Erro ao tentar preencher a Proposta", Toast.LENGTH_LONG).show();
        }

    }
    }
