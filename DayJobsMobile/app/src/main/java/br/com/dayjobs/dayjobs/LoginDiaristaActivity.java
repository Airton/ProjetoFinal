package br.com.dayjobs.dayjobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import br.com.dayjobs.dayjobs.ClasseBasica.Diarista;

/**
 * Created by Brendo Santiago on 21/05/2017.
 */

public class LoginDiaristaActivity extends AppCompatActivity {

    private static String METHOD_NAME = "";
    String tipoLogin;
    TextView txt_EsqueceuASenha;
    ImageButton imageButton;

    Diarista objetoDiarista;
    SoapObject result;
    String retorno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login_diarista);

        Intent intent = getIntent();
        retorno = intent.getStringExtra("retorno");
        if (retorno != null) {
            Toast.makeText(LoginDiaristaActivity.this, "Em instantes sua senha estará no e-mail cadastrado!", Toast.LENGTH_LONG).show();
        }

        imageButton = (ImageButton) findViewById(R.id.imageViewVoltar);
        //Voltar a tela anterior
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txt_EsqueceuASenha = (TextView) findViewById(R.id.link_esqueceu_senha);
        //Abrir a tela esquecer senha
        txt_EsqueceuASenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent esqueceuSenha = new Intent(LoginDiaristaActivity.this, EsqueceuSenhaDiarista.class);
                startActivity(esqueceuSenha);
                finish();
            }
        });
    }

    public void EnviarOnClick(View v) {
        Thread nt = new Thread() {
            EditText email = (EditText) findViewById(R.id.edt_email);
            EditText senha = (EditText) findViewById(R.id.edt_senha);

            @Override
            public void run() {
                String SOAP_ACTION = "http://webservice.dayjobs.com.br/LoginNoSistemaDiarista";
                String NAMESPACE = "http://webservice.dayjobs.com.br/";
                METHOD_NAME = "LoginNoSistemaDiarista";
                String URL = "http://webservice.dayjobs.com.br/WebService.asmx?WSDL";

                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                // requisição passando o parametro do metodo e o valor a colocar.
                request.addProperty("email", email.getText().toString());
                request.addProperty("senha", senha.getText().toString());

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;

                envelope.setOutputSoapObject(request);

                HttpTransportSE transporte = new HttpTransportSE(URL);

                try {
                    transporte.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();

                    ObjDiaristaPreencher();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (objetoDiarista.getID_DIARISTA() != 0) {
                            Intent intent = new Intent(LoginDiaristaActivity.this, TelaPrincipalDiarista.class);
                            intent.putExtra("diarista", objetoDiarista);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(LoginDiaristaActivity.this, "Email e/ou senha incorreto(s)!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        };
        nt.start();
    }

    public void ObjDiaristaPreencher() {
        try {
            objetoDiarista = new Diarista();
            if (result.hasProperty("NomeDiarista")) {
                objetoDiarista.setID_DIARISTA(Integer.parseInt(result.getPropertySafelyAsString("IdDiarista").toString()));
                objetoDiarista.setFOTO_DIARISTA(result.getPropertySafelyAsString("FotoDiarista").toString());
                objetoDiarista.setNOME_DIARISTA(result.getPropertySafelyAsString("NomeDiarista").toString());
                objetoDiarista.setCPF_DIARISTA(result.getPropertySafelyAsString("CpfDiarista").toString());
                objetoDiarista.setTELEFONE_DIARISTA(result.getPropertySafelyAsString("TelefoneDiarista").toString());
                objetoDiarista.setCELULAR_DIARISTA(result.getPropertySafelyAsString("CelularDiarista").toString());
                objetoDiarista.setDATANASCIMENTO_DIARISTA(result.getPropertySafelyAsString("DataNascimentoDiarista").toString());
                objetoDiarista.setDESCRICAO_DIARISTA(result.getPropertySafelyAsString("DescricaoDiarista").toString());
                objetoDiarista.setCEP_DIARISTA(result.getPropertySafelyAsString("CepDiarista").toString());
                objetoDiarista.setLOGRADOURO_DIARISTA(result.getPropertySafelyAsString("LogradouroDiarista").toString());
                objetoDiarista.setNUMERO_DIARISTA(result.getPropertySafelyAsString("NumeroDiarista").toString());
                objetoDiarista.setCOMPLEMENTO_DIARISTA(result.getPropertySafelyAsString("ComplementoDiarista").toString());
                objetoDiarista.setBAIRRO_DIARISTA(result.getPropertySafelyAsString("BairroDiarista").toString());
                objetoDiarista.setCIDADE_DIARISTA(result.getPropertySafelyAsString("CidadeDiarista").toString());
                objetoDiarista.setUF_DIARISTA(result.getPropertySafelyAsString("UfDiarista").toString());
                objetoDiarista.setLATITUDE_DIARISTA(result.getPropertySafelyAsString("LatitudeDiarista").toString());
                objetoDiarista.setLONGITUDE_DIARISTA(result.getPropertySafelyAsString("LongitudeDiarista").toString());
                objetoDiarista.setNOMEBENEFICIARIO_DIARISTA(result.getPropertyAsString("NomeBeneficiarioDiarista").toString());
                objetoDiarista.setCONTA_DIARISTA(result.getPropertySafelyAsString("ContaDiarista").toString());
                objetoDiarista.setAGENCIA_DIARISTA(result.getPropertySafelyAsString("AgenciaDiarista").toString());
                objetoDiarista.setEMAIL_DIARISTA(result.getPropertySafelyAsString("EmailDiarista").toString());
            } else {
                objetoDiarista.setID_DIARISTA(0);
            }
        } catch (Exception ex) {
            Toast.makeText(LoginDiaristaActivity.this, "Erro ao tentar preencher a diarista", Toast.LENGTH_LONG).show();
        }
    }

    //Botão Home para executar o método finish() da activity
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //O método finish() vai encerrar essa activity
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}