package br.com.dayjobs.dayjobs.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.dayjobs.dayjobs.ClasseBasica.Diarista;
import br.com.dayjobs.dayjobs.ClasseBasica.Proposta;
import br.com.dayjobs.dayjobs.R;

/**
 * Created by Carlos Eduardo on 09/05/2017.
 */

public class DiaristaAdapter extends ArrayAdapter<Proposta> {

    public String linkDaImagem = "https://image.tmdb.org/t/p/w500/";

    public DiaristaAdapter(Context context, List<Proposta> proposta) {
        super(context, 0, proposta);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Diarista diarista = new Diarista();
        Proposta proposta = getItem(position);
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.item_diarista, parent, false);
        }
        holder = new ViewHolder();
        holder.ESTRELA = (RatingBar) convertView.findViewById(R.id.item_estrelas);
        holder.FOTO_DIARISTA = (ImageView) convertView.findViewById(R.id.item_foto);
        holder.LOGRADOURO_DIARISTA = (TextView) convertView.findViewById(R.id.item_logradouro);
        holder.NOME = (TextView) convertView.findViewById(R.id.item_nomeDiarista);
        holder.Valor = (TextView) convertView.findViewById(R.id.item_valorProposta);
        Glide.with(getContext())
                //Carregar a url desejada e ele fazem backgroud
                .load(linkDaImagem + diarista
                        .getFOTO_DIARISTA())
                //Setando a imageView
                .into(holder.FOTO_DIARISTA);
        holder.NOME.setText("Nome: " + diarista.getNOME_DIARISTA());
        holder.LOGRADOURO_DIARISTA.setText("Logradouro: "+ diarista.getLOGRADOURO_DIARISTA());
        //holder.ESTRELA.setRating(diarista.getESTRELA());
        holder.Valor.setText("Valor: "+ proposta.getVALORTOTAL_PROPOSTA()+"R$");
        return convertView;
    }

    static class ViewHolder {
        ImageView FOTO_DIARISTA;
        TextView LOGRADOURO_DIARISTA;
        TextView NOME;
        RatingBar ESTRELA;
        TextView Valor;
        TextView LATITUDE_DIARISTA;
        TextView LONGITUDE_DIARISTA;
    }
}
