package br.com.dayjobs.dayjobs;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.dayjobs.dayjobs.ClasseBasica.Cliente;
import br.com.dayjobs.dayjobs.ClasseBasica.Diarista;

public class MapsFragment extends SupportMapFragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener {
    public MapsFragment() {

    }

    private static final String TAG = "MapsFragment";

    private GoogleMap mMap;
    LatLng mOrigemDiarista, mOrigemCliente, mOrigem;
    private LocationManager locationManager;
    Diarista lista, diarista, objeto;
    Cliente cliente;
    BitmapDescriptor icon;
    List<Diarista> listaDiarista;
    SoapObject result, resultDiarista;
    private static String METHOD_NAME = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMapAsync(this);
        Intent intent = getActivity().getIntent();
        setHasOptionsMenu(true);
        diarista = intent.getParcelableExtra("diarista");
        cliente = intent.getParcelableExtra("cliente");
        ReceberLista();
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ReceberLista();
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            mMap = googleMap;
            mMap.setOnMapClickListener(this);
            mMap.getUiSettings().setZoomControlsEnabled(true);

            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            }
            if (cliente != null){
                AtualizarMapaCliente();
            } else {
                AtualizarMapaDiarista();
            }
        } catch (SecurityException ex) {
            Log.e(TAG, "Erro", ex);
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Toast.makeText(getContext(), "Coordenadas: " + latLng.toString(),
                Toast.LENGTH_SHORT);
    }

    private void AtualizarMapaCliente() {
        if (cliente.getLATITUDE_CLIENTE().equals("anyType{}") || cliente.getLONGITUDE_CLIENTE().equals("anyType{}")) {
            cliente.setLATITUDE_CLIENTE("-8.1515520");
            cliente.setLONGITUDE_CLIENTE("-34.9199220");
        }
        mOrigemCliente = new LatLng(Double.parseDouble(cliente.getLATITUDE_CLIENTE().trim()), Double.parseDouble(cliente.getLONGITUDE_CLIENTE().trim()));
        // Move a câmera para Framework System com zoom 15.
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mOrigemCliente, 15));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);
        icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_cliente);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.addMarker(new MarkerOptions()
                .position(mOrigemCliente)
                .icon(icon)
                .title(cliente.getNOME_CLIENTE()));
        //.snippet(cliente.getEMAIL_CLIENTE()));

        for (int cont = 0; cont < listaDiarista.size(); cont++) {
            lista = new Diarista();
            lista = listaDiarista.get(cont);
            if (lista.getSTATUS_DIARISTA().equals("ONLINE")) {
                if (lista.getLATITUDE_DIARISTA().equals("anyType{}") || lista.getLONGITUDE_DIARISTA().equals("anyType{}")) {
                    mOrigemDiarista = new LatLng(Double.parseDouble("-8.1515520"), Double.parseDouble("-34.9199220"));
                    icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_diarista);
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    mMap.getUiSettings().setZoomControlsEnabled(true);
                    mMap.addMarker(new MarkerOptions()
                            .position(mOrigemDiarista)
                            .icon(icon)
                            .title(lista.getNOME_DIARISTA())
                            .snippet(lista.getCELULAR_DIARISTA()));
                } else {
                    mOrigemDiarista = new LatLng(Double.parseDouble(lista.getLATITUDE_DIARISTA().trim()), Double.parseDouble(lista.getLONGITUDE_DIARISTA().trim()));
                    icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_diarista);
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    mMap.getUiSettings().setZoomControlsEnabled(true);
                    mMap.addMarker(new MarkerOptions()
                            .position(mOrigemDiarista)
                            .icon(icon)
                            .title(lista.getNOME_DIARISTA())
                            .snippet(lista.getCELULAR_DIARISTA()));
                }
            }
        }
    }

    private void AtualizarMapaDiarista() {
        if (diarista.getLATITUDE_DIARISTA().equals("anyType{}") || diarista.getLONGITUDE_DIARISTA().equals("anyType{}")) {
            diarista.setLATITUDE_DIARISTA("-8.1515520");
            diarista.setLONGITUDE_DIARISTA("-34.9199220");
        }
        mOrigemDiarista = new LatLng(Double.parseDouble(diarista.getLATITUDE_DIARISTA().trim()), Double.parseDouble(diarista.getLONGITUDE_DIARISTA().trim()));
        // Move a câmera para Framework System com zoom 15.
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mOrigemDiarista, 15));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);
        icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_diarista);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.addMarker(new MarkerOptions()
                .position(mOrigemDiarista)
                .icon(icon)
                .title(diarista.getNOME_DIARISTA()));
                //.snippet(cliente.getEMAIL_CLIENTE()));
    }

    public void ReceberLista() {
        listaDiarista = new ArrayList<>();
        Thread thread = new Thread() {
            @Override
            public void run() {
                String SOAP_ACTION = "http://webservice.dayjobs.com.br/ListarDiarista";
                String NAMESPACE = "http://webservice.dayjobs.com.br/";
                METHOD_NAME = "ListarDiarista";
                String URL = "http://webservice.dayjobs.com.br/WebService.asmx?WSDL";

                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;

                envelope.setOutputSoapObject(request);

                HttpTransportSE transporte = new HttpTransportSE(URL);

                try {
                    transporte.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();

                    for (int cont = 0; result.getPropertyCount() > cont; cont++) {
                        resultDiarista = (SoapObject) result.getProperty(cont);
                        objeto = new Diarista();
                        ObjDiaristaPreencher();
                        listaDiarista.add(objeto);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    public void ObjDiaristaPreencher() {
        try {
            if (resultDiarista.hasProperty("NomeDiarista")) {
                objeto.setID_DIARISTA(Integer.parseInt(resultDiarista.getPropertyAsString("IdDiarista").toString()));
                objeto.setFOTO_DIARISTA(resultDiarista.getPropertySafelyAsString("FotoDiarista").toString());
                objeto.setNOME_DIARISTA(resultDiarista.getPropertyAsString("NomeDiarista").toString());
                objeto.setCPF_DIARISTA(resultDiarista.getPropertySafelyAsString("CpfDiarista").toString());
                objeto.setTELEFONE_DIARISTA(resultDiarista.getPropertySafelyAsString("TelefoneDiarista").toString());
                objeto.setCELULAR_DIARISTA(resultDiarista.getPropertySafelyAsString("CelularDiarista").toString());
                objeto.setDATANASCIMENTO_DIARISTA(resultDiarista.getPropertySafelyAsString("DataNascimentoDiarista").toString());
                objeto.setDESCRICAO_DIARISTA(resultDiarista.getPropertySafelyAsString("DescricaoDiarista").toString());
                objeto.setCEP_DIARISTA(resultDiarista.getPropertySafelyAsString("CepDiarista").toString());
                objeto.setLOGRADOURO_DIARISTA(resultDiarista.getPropertySafelyAsString("LogradouroDiarista").toString());
                objeto.setNUMERO_DIARISTA(resultDiarista.getPropertySafelyAsString("NumeroDiarista").toString());
                objeto.setCOMPLEMENTO_DIARISTA(resultDiarista.getPropertySafelyAsString("ComplementoDiarista").toString());
                objeto.setBAIRRO_DIARISTA(resultDiarista.getPropertySafelyAsString("BairroDiarista").toString());
                objeto.setCIDADE_DIARISTA(resultDiarista.getPropertySafelyAsString("CidadeDiarista").toString());
                objeto.setUF_DIARISTA(resultDiarista.getPropertySafelyAsString("UfDiarista").toString());
                objeto.setLATITUDE_DIARISTA(resultDiarista.getPropertySafelyAsString("LatitudeDiarista").toString());
                objeto.setLONGITUDE_DIARISTA(resultDiarista.getPropertySafelyAsString("LongitudeDiarista").toString());
                objeto.setNOMEBENEFICIARIO_DIARISTA(resultDiarista.getPropertySafelyAsString("NomeBeneficiarioDiarista").toString());
                objeto.setCONTA_DIARISTA(resultDiarista.getPropertySafelyAsString("ContaDiarista").toString());
                objeto.setAGENCIA_DIARISTA(resultDiarista.getPropertySafelyAsString("AgenciaDiarista").toString());
                objeto.setEMAIL_DIARISTA(resultDiarista.getPropertySafelyAsString("EmailDiarista").toString());
                objeto.setSENHA_DIARISTA(resultDiarista.getPropertySafelyAsString("SenhaDiarista").toString());
                objeto.setSTATUS_DIARISTA(resultDiarista.getPropertySafelyAsString("StatusDiarista").toString());
            }
        } catch (Exception ex) {
            Toast.makeText(getActivity(), "Erro ao tentar preencher a Diarista", Toast.LENGTH_LONG).show();
        }
    }
}
