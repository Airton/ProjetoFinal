package br.com.dayjobs.dayjobs.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.dayjobs.dayjobs.ClasseBasica.Diarista;
import br.com.dayjobs.dayjobs.R;
import br.com.dayjobs.dayjobs.fragmentListaSelecionada;

/**
 * Created by Carlos Eduardo on 30/05/2017.
 */

public class DiaristaSelecionadaAdapter extends ArrayAdapter<Diarista> {
    public String linkDaImagem = "https://image.tmdb.org/t/p/w500/";
    ViewHolder holder;
    Diarista diarista;
    int checkqtd = 0;
    List<Diarista> ListDiarista, listaSelecionada;
    public DiaristaSelecionadaAdapter(Context context, List<Diarista> Diarista) {

        super(context, 0, Diarista);
        ListDiarista = Diarista;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
         diarista = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.item_diarista_selecionada, parent, false);
        }
        holder = new ViewHolder();
        holder.ESTRELA = (RatingBar) convertView.findViewById(R.id.item_estrelasSelecionada);
        holder.FOTO_DIARISTA = (ImageView) convertView.findViewById(R.id.img_diaristaSelecionada);
        holder.DESCRICAO = (TextView) convertView.findViewById(R.id.item_logradouro);
        holder.NOME = (TextView) convertView.findViewById(R.id.item_nomeDiarista);
        holder.Valor = (TextView) convertView.findViewById(R.id.item_valorProposta);
        holder.RADIOBUTTON = (RadioButton) convertView.findViewById(R.id.radioButton);
        holder.RADIOBUTTON.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    ListDiarista.get(position).setCHECK(true);
                    Intent selecionada = new Intent(getContext(), fragmentListaSelecionada.class);
                    selecionada.putExtra("Diarista",diarista);
                } else {
                    ListDiarista.get(position).setCHECK(false);
                }
            }
        });

        Glide.with(getContext()).load(diarista.getFOTO_DIARISTA()).into(holder.FOTO_DIARISTA);


        holder.NOME.setText("Nome: " + diarista.getNOME_DIARISTA());
        holder.DESCRICAO.setText("Descriçao: "+ diarista.getLOGRADOURO_DIARISTA());

        return convertView;
    }

    static class ViewHolder {
        //ImageView FOTO_DIARISTA;
        RadioButton RADIOBUTTON;
        ImageView FOTO_DIARISTA;
        TextView DESCRICAO;
        TextView NOME;
        RatingBar ESTRELA;
        TextView Valor;
    }
}
