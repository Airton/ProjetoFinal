package br.com.dayjobs.dayjobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class EscolhaLoginActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnLoginDiarista;
    Button btnLoginCliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide(); //Mágicaaaaa
        setContentView(R.layout.activity_escolha_login);

        btnLoginCliente = (Button) findViewById(R.id.btn_login_cliente);
        btnLoginCliente.setOnClickListener(this);

        btnLoginDiarista = (Button) findViewById(R.id.btn_login_diarista);
        btnLoginDiarista.setOnClickListener(this);
    }

    @Override
    public void onClick (View botao) {
        if(botao.getId() == findViewById(R.id.btn_login_cliente).getId()) {
            Intent cliente = new Intent(this, LoginClienteActivity.class);
            startActivity(cliente);
        } else {
            Intent diarista = new Intent(this, LoginDiaristaActivity.class);
            startActivity(diarista);
        }
    }
}