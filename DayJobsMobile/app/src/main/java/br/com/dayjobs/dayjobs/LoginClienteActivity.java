package br.com.dayjobs.dayjobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import br.com.dayjobs.dayjobs.ClasseBasica.Cliente;

/**
 * Created by Brendo Santiago on 07/04/2017.
 */

public class LoginClienteActivity extends AppCompatActivity {

    private static String METHOD_NAME = "";
    TextView txt_EsqueceuASenha;
    ImageButton imageButton;

    Cliente objetoCliente;
    SoapObject result;
    String retorno;

    private LoginButton loginButton;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login_cliente);

        Intent intent = getIntent();
        retorno = intent.getStringExtra("retorno");
        if (retorno != null) {
            Toast.makeText(LoginClienteActivity.this, "Em instantes sua senha estará no e-mail cadastrado!", Toast.LENGTH_LONG).show();
        }

        callbackManager = CallbackManager.Factory.create();

        //Botão de Login com o Facebook
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                goMainScreen();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), R.string.cancel_login, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), R.string.error_login, Toast.LENGTH_SHORT).show();
            }
        });

        imageButton = (ImageButton) findViewById(R.id.imageViewVoltar);
        //Voltar a tela anterior
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txt_EsqueceuASenha = (TextView) findViewById(R.id.link_esqueceu_senha);
        //Abrir a tela esquecer senha
        txt_EsqueceuASenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent esqueceuSenha = new Intent(LoginClienteActivity.this, EsqueceuSenhaCliente.class);
                startActivity(esqueceuSenha);
                finish();
            }
        });
    }

    //Métodos Facebook - Início
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void goMainScreen() {
        Intent intent = new Intent(this, TelaPrincipalComFacebook.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    //Métodos Facebook - Fim

    public void EnviarOnClick(View v) {
        Thread nt = new Thread() {
            EditText email = (EditText) findViewById(R.id.edt_email);
            EditText senha = (EditText) findViewById(R.id.edt_senha);

            @Override
            public void run() {
                String SOAP_ACTION = "http://webservice.dayjobs.com.br/LoginNoSistemaCliente";
                String NAMESPACE = "http://webservice.dayjobs.com.br/";
                METHOD_NAME = "LoginNoSistemaCliente";
                String URL = "http://webservice.dayjobs.com.br/WebService.asmx?WSDL";

                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                // requisição passando o parametro do metodo e o valor a colocar.
                request.addProperty("email", email.getText().toString());
                request.addProperty("senha", senha.getText().toString());

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;

                envelope.setOutputSoapObject(request);

                HttpTransportSE transporte = new HttpTransportSE(URL);

                try {
                    transporte.call(SOAP_ACTION, envelope);
                    //SoapPrimitive resultado = (SoapPrimitive) envelope.getResponse();
                    result = (SoapObject) envelope.getResponse();

                    ObjClientePreencher();

                    //result1 = ( Integer.parseInt(result.getProperty("IdCliente").toString()));
                    //result2 = ( Integer.parseInt(result.getProperty("IdDiarista").toString()));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (objetoCliente.getID_CLIENTE() != 0) {
                            Intent intent = new Intent(LoginClienteActivity.this, TelaPrincipalCliente.class);
                            intent.putExtra("cliente", objetoCliente);
                            Intent intentCliente = new Intent(LoginClienteActivity.this,TelaProposta.class);
                            intentCliente.putExtra("cliente",objetoCliente);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(LoginClienteActivity.this, "Email e/ou senha incorreto(s)", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        };
        nt.start();
    }

    public void ObjClientePreencher() {
        try {
            objetoCliente = new Cliente();
            if (result.hasProperty("NomeCliente")) {
                objetoCliente.setID_CLIENTE(Integer.parseInt(result.getPropertySafelyAsString("IdCliente").toString()));
                objetoCliente.setFOTO_CLIENTE(result.getPropertySafelyAsString("FotoCliente").toString());
                objetoCliente.setNOME_CLIENTE(result.getPropertySafelyAsString("NomeCliente").toString());
                objetoCliente.setCELULAR_CLIENTE(result.getPropertySafelyAsString("CelularCliente").toString());
                objetoCliente.setDATANASCIMENTO_CLIENTE(result.getPropertySafelyAsString("DataNascimentoCliente").toString());
                objetoCliente.setCEP_CLIENTE(result.getPropertySafelyAsString("CepCliente").toString());
                objetoCliente.setLOGRADOURO_CLIENTE(result.getPropertySafelyAsString("LogradouroCliente").toString());
                objetoCliente.setNUMERO_CLIENTE(result.getPropertySafelyAsString("NumeroCliente").toString());
                objetoCliente.setCOMPLEMENTO_CLIENTE(result.getPropertySafelyAsString("ComplementoCliente").toString());
                objetoCliente.setBAIRRO_CLIENTE(result.getPropertySafelyAsString("BairroCliente").toString());
                objetoCliente.setCIDADE_CLIENTE(result.getPropertySafelyAsString("CidadeCliente").toString());
                objetoCliente.setUF_CLIENTE(result.getPropertySafelyAsString("UfCliente").toString());
                objetoCliente.setLATITUDE_CLIENTE(result.getPropertySafelyAsString("LatitudeCliente").toString());
                objetoCliente.setLONGITUDE_CLIENTE(result.getPropertySafelyAsString("LongitudeCliente").toString());
                objetoCliente.setEMAIL_CLIENTE(result.getPropertySafelyAsString("EmailCliente").toString());
                objetoCliente.setSENHA_CLIENTE(result.getPropertySafelyAsString("SenhaCliente").toString());
            } else {
                objetoCliente.setID_CLIENTE(0);
            }
        } catch (Exception ex) {
            Toast.makeText(LoginClienteActivity.this, "Erro ao tentar preencher o cliente", Toast.LENGTH_LONG).show();
        }
    }

    //Botão Home para executar o método finish() da activity
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //O método finish() vai encerrar essa activity
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}