package br.com.dayjobs.dayjobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import br.com.dayjobs.dayjobs.ClasseBasica.Diarista;

public class PerfilDiaristaActivity extends AppCompatActivity {

    protected DrawerLayout drawerLayout;
    Diarista diarista;
    Boolean result;
    private static String METHOD_NAME = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_diarista);

        Intent it = getIntent();
        diarista = it.getParcelableExtra("diarista");

        PreencherTelaDiarista();
    }

    public void PreencherTelaDiarista() {
        ImageView imageView = (ImageView) findViewById(R.id.img_perfil_diarista);
        EditText input_nomeDiarista = (EditText) findViewById(R.id.edt_nome_diarista);
        EditText input_cpfDiarista = (EditText) findViewById(R.id.edt_cpf_diarista);
        EditText input_telefoneDiarista = (EditText) findViewById(R.id.edt_telefone_diarista);
        EditText input_celularDiarista = (EditText) findViewById(R.id.edt_celular_diarista);
        EditText input_dataNascimentoDiarista = (EditText) findViewById(R.id.edt_data_nascimento_diarista);
        EditText input_descricaoDiarista = (EditText) findViewById(R.id.edt_descricao_diarista);
        EditText input_cepDiarista = (EditText) findViewById(R.id.edt_cep_diarista);
        EditText input_logradouroDiarista = (EditText) findViewById(R.id.edt_logradouro_diarista);
        EditText input_numeroDiarista = (EditText) findViewById(R.id.edt_numero_diarista);
        EditText input_complementoDiarista = (EditText) findViewById(R.id.edt_complemento_diarista);
        EditText input_bairroDiarista = (EditText) findViewById(R.id.edt_bairro_diarista);
        EditText input_cidadeDiarista = (EditText) findViewById(R.id.edt_cidade_diarista);
        EditText input_ufDiarista = (EditText) findViewById(R.id.edt_uf_diarista);
        EditText input_nomeBeneficiarioDiarista = (EditText) findViewById(R.id.edt_nome_beneficiario_diarista);
        EditText input_contaDiarista = (EditText) findViewById(R.id.edt_conta_diarista);
        EditText input_agenciaDiarista = (EditText) findViewById(R.id.edt_agencia_diarista);
        EditText input_email = (EditText) findViewById(R.id.edt_email_diarista);

        Glide.with(this).load(diarista.getFOTO_DIARISTA()).into(imageView);
        input_nomeDiarista.setText(diarista.getNOME_DIARISTA());
        input_cpfDiarista.setText(diarista.getCPF_DIARISTA());
        if (diarista.getTELEFONE_DIARISTA().equals("anyType{}")) {
            input_telefoneDiarista.setText("");
        } else {
            input_telefoneDiarista.setText(diarista.getTELEFONE_DIARISTA());
        }
        input_celularDiarista.setText(diarista.getCELULAR_DIARISTA());
        if (diarista.getDATANASCIMENTO_DIARISTA().equals("anyType{}")) {
            input_dataNascimentoDiarista.setText("");
        } else {
            input_dataNascimentoDiarista.setText(diarista.getDATANASCIMENTO_DIARISTA());
        }
        if (diarista.getDESCRICAO_DIARISTA().equals("anyType{}")) {
            input_descricaoDiarista.setText("");
        } else {
            input_descricaoDiarista.setText(diarista.getDESCRICAO_DIARISTA());
        }
        if (diarista.getCEP_DIARISTA().equals("anyType{}")) {
            input_cepDiarista.setText("");
        } else {
            input_cepDiarista.setText(diarista.getCEP_DIARISTA());
        }
        if (diarista.getLOGRADOURO_DIARISTA().equals("anyType{}")) {
            input_logradouroDiarista.setText("");
        } else {
            input_logradouroDiarista.setText(diarista.getLOGRADOURO_DIARISTA());
        }
        if (diarista.getNUMERO_DIARISTA().equals("anyType{}")) {
            input_numeroDiarista.setText("");
        } else {
            input_numeroDiarista.setText(diarista.getNUMERO_DIARISTA());
        }
        if (diarista.getCOMPLEMENTO_DIARISTA().equals("anyType{}")) {
            input_complementoDiarista.setText("");
        } else {
            input_complementoDiarista.setText(diarista.getCOMPLEMENTO_DIARISTA());
        }
        if (diarista.getBAIRRO_DIARISTA().equals("anyType{}")) {
            input_bairroDiarista.setText("");
        } else {
            input_bairroDiarista.setText(diarista.getBAIRRO_DIARISTA());
        }
        if (diarista.getCIDADE_DIARISTA().equals("anyType{}")) {
            input_cidadeDiarista.setText("");
        } else {
            input_cidadeDiarista.setText(diarista.getCIDADE_DIARISTA());
        }
        if (diarista.getUF_DIARISTA().equals("anyType{}")) {
            input_ufDiarista.setText("");
        } else {
            input_ufDiarista.setText(diarista.getUF_DIARISTA());
        }
        if (diarista.getNOMEBENEFICIARIO_DIARISTA().equals("anyType{}")) {
            input_nomeBeneficiarioDiarista.setText("");
        } else {
            input_nomeBeneficiarioDiarista.setText(diarista.getNOMEBENEFICIARIO_DIARISTA());
        }
        if (diarista.getCONTA_DIARISTA().equals("anyType{}")) {
            input_contaDiarista.setText("");
        } else {
            input_contaDiarista.setText(diarista.getCONTA_DIARISTA());
        }
        if (diarista.getAGENCIA_DIARISTA().equals("anyType{}")) {
            input_agenciaDiarista.setText("");
        } else {
            input_agenciaDiarista.setText(diarista.getAGENCIA_DIARISTA());
        }
        input_email.setText(diarista.getEMAIL_DIARISTA());
    }
}
