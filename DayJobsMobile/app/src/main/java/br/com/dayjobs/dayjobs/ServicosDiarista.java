package br.com.dayjobs.dayjobs;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class ServicosDiarista extends AppCompatActivity  {
    ViewPager mViewPager;
    SelectorPageAdapter mselectorPageAdapter;
    Toolbar toolbar;

    private FragmentManager fragmentManager;
    DiaServicosListFragment diaServicosListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.servicos_diarista);
        //setContentView(R.layout.servicos_diarista);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       buildViewPager();
    }

    private void buildViewPager() {
        mViewPager = (ViewPager) findViewById(R.id.container_diarista);
        mselectorPageAdapter = new SelectorPageAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mselectorPageAdapter);

        TabLayout tab = (TabLayout) findViewById(R.id.tabe_diarista);
        tab.setupWithViewPager(mViewPager);
    }

    public class SelectorPageAdapter extends FragmentPagerAdapter {
        public SelectorPageAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    diaServicosListFragment =  new DiaServicosListFragment();
                    return diaServicosListFragment;
                case 1:
                default:
                    diaServicosListFragment =  new DiaServicosListFragment();
                    return diaServicosListFragment;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
        @Override
        public CharSequence getPageTitle(int Position) {
            switch (Position) {
                case 0:
                    return "Solicitados";
                case 1:
                default:
                    return "Histórico";
            }
        }
    }
}