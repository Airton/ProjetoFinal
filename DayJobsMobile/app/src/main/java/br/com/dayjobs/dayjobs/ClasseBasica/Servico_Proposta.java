package br.com.dayjobs.dayjobs.ClasseBasica;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by P on 28/04/2017.
 */

public class Servico_Proposta implements Parcelable{

    public Servico_Proposta(Parcel in){

    }

    public Servico_Proposta(String teste) {

        ID_SERVICO = 4;
        ID_PROPOSTA = 3;

    }


    public static final Creator<Servico_Proposta> CREATOR = new Creator<Servico_Proposta>() {
        @Override
        public Servico_Proposta createFromParcel(Parcel in) {
            return new Servico_Proposta(in);
        }

        @Override
        public Servico_Proposta[] newArray(int size) {
            return new Servico_Proposta[size];
        }
    };



    private int ID_SERVICO;
    private int ID_PROPOSTA;




    public int getID_SERVICO() {
        return ID_SERVICO;
    }

    public void setID_SERVICO(int ID_SERVICO) {
        this.ID_SERVICO = ID_SERVICO;
    }

    public int getID_PROPOSTA() {
        return ID_PROPOSTA;
    }

    public void setID_PROPOSTA(int ID_PROPOSTA) {
        this.ID_PROPOSTA = ID_PROPOSTA;
    }




    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID_SERVICO);
        dest.writeInt(ID_PROPOSTA);

    }



}
