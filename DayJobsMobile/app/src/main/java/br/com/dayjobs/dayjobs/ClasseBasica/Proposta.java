package br.com.dayjobs.dayjobs.ClasseBasica;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Carlos Eduardo on 10/05/2017.
 */

public class Proposta implements Parcelable {

public Proposta(){

}
    public Proposta(String teste) {
        Calendar  cal = Calendar.getInstance();
          ID_PROPOSTA = 1;
          VALORTOTAL_PROPOSTA = 25.50;
          HORARIOFIM_PROPOSTA = cal.getTime();
          SITUACAO_PROPOSTA = "EM ABERTO";
          CEP_PROPOSTA = "51010-140";
          LOGRADOURO_PROPOSTA = "RUA DAGBERTO PIRES";
          NUMERO_PROPOSTA = "22";
          BAIRRO_PROPOSTA = "BRASILIA TEIMOSA";
          CIDADE_PROSPOSTA = "RECIFE";
          UF_PROPOSTA = "PE";
          LATITUDE_PROPOSTA = "0";
          LONGITUDE_PROPOTA = "0";
          HORARIOINICIO_PROPOSTA = cal.getTime();
          ENDERECO_PROPOSTA = "RUA ABC";
          COMPLEMENTO_PROPOSTA = "COMPLEMENTO";
          TOTAL_PROPOSTA = 50.00;
          STATUS_PROPOSTA = "PENDENTE";
          AVALIACAO_PROPOSTA = "TEXTO";
          CLASSIFICACAO_PROPOSTA = 3;
          ID_DIARISTA = 1;
          ID_CLIENTE = 1;
    }
    private int ID_PROPOSTA;
    private double VALORTOTAL_PROPOSTA;
    private Date HORARIOFIM_PROPOSTA;
    private String SITUACAO_PROPOSTA;
    private String CEP_PROPOSTA;
    private String LOGRADOURO_PROPOSTA;
    private String NUMERO_PROPOSTA;
    private String BAIRRO_PROPOSTA;
    private String CIDADE_PROSPOSTA;
    private String UF_PROPOSTA;
    private String LATITUDE_PROPOSTA;
    private String LONGITUDE_PROPOTA;
    private Date HORARIOINICIO_PROPOSTA;
    private String ENDERECO_PROPOSTA;
    private String COMPLEMENTO_PROPOSTA;
    private double TOTAL_PROPOSTA;
    private String STATUS_PROPOSTA;
    private String AVALIACAO_PROPOSTA;
    private double CLASSIFICACAO_PROPOSTA;
    private int ID_DIARISTA;
    private int ID_CLIENTE;

    protected Proposta(Parcel in) {
        ID_PROPOSTA = in.readInt();
        VALORTOTAL_PROPOSTA = in.readDouble();
        SITUACAO_PROPOSTA = in.readString();
        CEP_PROPOSTA = in.readString();
        LOGRADOURO_PROPOSTA = in.readString();
        NUMERO_PROPOSTA = in.readString();
        BAIRRO_PROPOSTA = in.readString();
        CIDADE_PROSPOSTA = in.readString();
        UF_PROPOSTA = in.readString();
        LATITUDE_PROPOSTA = in.readString();
        LONGITUDE_PROPOTA = in.readString();
        ENDERECO_PROPOSTA = in.readString();
        COMPLEMENTO_PROPOSTA = in.readString();
        TOTAL_PROPOSTA = in.readDouble();
        STATUS_PROPOSTA = in.readString();
        AVALIACAO_PROPOSTA = in.readString();
        CLASSIFICACAO_PROPOSTA = in.readDouble();
        ID_DIARISTA = in.readInt();
        ID_CLIENTE = in.readInt();
    }

    public static final Creator<Proposta> CREATOR = new Creator<Proposta>() {
        @Override
        public Proposta createFromParcel(Parcel in) {
            return new Proposta(in);
        }

        @Override
        public Proposta[] newArray(int size) {
            return new Proposta[size];
        }
    };

    public int getID_PROPOSTA() {
        return ID_PROPOSTA;
    }

    public void setID_PROPOSTA(int ID_PROPOSTA) {
        this.ID_PROPOSTA = ID_PROPOSTA;
    }

    public double getVALORTOTAL_PROPOSTA() {
        return VALORTOTAL_PROPOSTA;
    }

    public void setVALORTOTAL_PROPOSTA(double VALORTOTAL_PROPOSTA) {
        this.VALORTOTAL_PROPOSTA = VALORTOTAL_PROPOSTA;
    }

    public Date getHORARIOFIM_PROPOSTA() {
        return HORARIOFIM_PROPOSTA;
    }

    public void setHORARIOFIM_PROPOSTA(Date HORARIOFIM_PROPOSTA) {
        this.HORARIOFIM_PROPOSTA = HORARIOFIM_PROPOSTA;
    }

    public String getSITUACAO_PROPOSTA() {
        return SITUACAO_PROPOSTA;
    }

    public void setSITUACAO_PROPOSTA(String SITUACAO_PROPOSTA) {
        this.SITUACAO_PROPOSTA = SITUACAO_PROPOSTA;
    }

    public String getCEP_PROPOSTA() {
        return CEP_PROPOSTA;
    }

    public void setCEP_PROPOSTA(String CEP_PROPOSTA) {
        this.CEP_PROPOSTA = CEP_PROPOSTA;
    }

    public String getLOGRADOURO_PROPOSTA() {
        return LOGRADOURO_PROPOSTA;
    }

    public void setLOGRADOURO_PROPOSTA(String LOGRADOURO_PROPOSTA) {
        this.LOGRADOURO_PROPOSTA = LOGRADOURO_PROPOSTA;
    }

    public String getNUMERO_PROPOSTA() {
        return NUMERO_PROPOSTA;
    }

    public void setNUMERO_PROPOSTA(String NUMERO_PROPOSTA) {
        this.NUMERO_PROPOSTA = NUMERO_PROPOSTA;
    }

    public String getBAIRRO_PROPOSTA() {
        return BAIRRO_PROPOSTA;
    }

    public void setBAIRRO_PROPOSTA(String BAIRRO_PROPOSTA) {
        this.BAIRRO_PROPOSTA = BAIRRO_PROPOSTA;
    }

    public String getCIDADE_PROSPOSTA() {
        return CIDADE_PROSPOSTA;
    }

    public void setCIDADE_PROSPOSTA(String CIDADE_PROSPOSTA) {
        this.CIDADE_PROSPOSTA = CIDADE_PROSPOSTA;
    }

    public String getUF_PROPOSTA() {
        return UF_PROPOSTA;
    }

    public void setUF_PROPOSTA(String UF_PROPOSTA) {
        this.UF_PROPOSTA = UF_PROPOSTA;
    }

    public String getLATITUDE_PROPOSTA() {
        return LATITUDE_PROPOSTA;
    }

    public void setLATITUDE_PROPOSTA(String LATITUDE_PROPOSTA) {
        this.LATITUDE_PROPOSTA = LATITUDE_PROPOSTA;
    }

    public String getLONGITUDE_PROPOTA() {
        return LONGITUDE_PROPOTA;
    }

    public void setLONGITUDE_PROPOTA(String LONGITUDE_PROPOTA) {
        this.LONGITUDE_PROPOTA = LONGITUDE_PROPOTA;
    }

    public Date getHORARIOINICIO_PROPOSTA() {
        return HORARIOINICIO_PROPOSTA;
    }

    public void setHORARIOINICIO_PROPOSTA(Date HORARIOINICIO_PROPOSTA) {
        this.HORARIOINICIO_PROPOSTA = HORARIOINICIO_PROPOSTA;
    }

    public String getENDERECO_PROPOSTA() {
        return ENDERECO_PROPOSTA;
    }

    public void setENDERECO_PROPOSTA(String ENDERECO_PROPOSTA) {
        this.ENDERECO_PROPOSTA = ENDERECO_PROPOSTA;
    }

    public String getCOMPLEMENTO_PROPOSTA() {
        return COMPLEMENTO_PROPOSTA;
    }

    public void setCOMPLEMENTO_PROPOSTA(String COMPLEMENTO_PROPOSTA) {
        this.COMPLEMENTO_PROPOSTA = COMPLEMENTO_PROPOSTA;
    }

    public double getTOTAL_PROPOSTA() {
        return TOTAL_PROPOSTA;
    }

    public void setTOTAL_PROPOSTA(double TOTAL_PROPOSTA) {
        this.TOTAL_PROPOSTA = TOTAL_PROPOSTA;
    }

    public String getSTATUS_PROPOSTA() {
        return STATUS_PROPOSTA;
    }

    public void setSTATUS_PROPOSTA(String STATUS_PROPOSTA) {
        this.STATUS_PROPOSTA = STATUS_PROPOSTA;
    }

    public String getAVALIACAO_PROPOSTA() {
        return AVALIACAO_PROPOSTA;
    }

    public void setAVALIACAO_PROPOSTA(String AVALIACAO_PROPOSTA) {
        this.AVALIACAO_PROPOSTA = AVALIACAO_PROPOSTA;
    }

    public double getCLASSIFICACAO_PROPOSTA() {
        return CLASSIFICACAO_PROPOSTA;
    }

    public void setCLASSIFICACAO_PROPOSTA(double CLASSIFICACAO_PROPOSTA) {
        this.CLASSIFICACAO_PROPOSTA = CLASSIFICACAO_PROPOSTA;
    }

    public int getID_DIARISTA() {
        return ID_DIARISTA;
    }

    public void setID_DIARISTA(int ID_DIARISTA) {
        this.ID_DIARISTA = ID_DIARISTA;
    }

    public int getID_CLIENTE() {
        return ID_CLIENTE;
    }

    public void setID_CLIENTE(int ID_CLIENTE) {
        this.ID_CLIENTE = ID_CLIENTE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID_PROPOSTA);
        dest.writeDouble(VALORTOTAL_PROPOSTA);
        dest.writeString(SITUACAO_PROPOSTA);
        dest.writeString(CEP_PROPOSTA);
        dest.writeString(LOGRADOURO_PROPOSTA);
        dest.writeString(NUMERO_PROPOSTA);
        dest.writeString(BAIRRO_PROPOSTA);
        dest.writeString(CIDADE_PROSPOSTA);
        dest.writeString(UF_PROPOSTA);
        dest.writeString(LATITUDE_PROPOSTA);
        dest.writeString(LONGITUDE_PROPOTA);
        dest.writeString(ENDERECO_PROPOSTA);
        dest.writeString(COMPLEMENTO_PROPOSTA);
        dest.writeDouble(TOTAL_PROPOSTA);
        dest.writeString(STATUS_PROPOSTA);
        dest.writeString(AVALIACAO_PROPOSTA);
        dest.writeDouble(CLASSIFICACAO_PROPOSTA);
        dest.writeInt(ID_DIARISTA);
        dest.writeInt(ID_CLIENTE);
    }
}
