package br.com.dayjobs.dayjobs.ClasseBasica;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Carlos Eduardo on 18/04/2017.
 */

/**
 * Update by Brendo Santiago on 25/05/2017.
 */

public class Diarista implements Parcelable {

    public Diarista() {

    }

    public Diarista(Diarista obj) {
        this.setNOME_DIARISTA(obj.getNOME_DIARISTA());
    }

    private int ID_DIARISTA;
    private String FOTO_DIARISTA;
    private String NOME_DIARISTA;
    private String CPF_DIARISTA;
    private String TELEFONE_DIARISTA;
    private String CELULAR_DIARISTA;
    private String DATANASCIMENTO_DIARISTA;
    private String DESCRICAO_DIARISTA;
    private String CEP_DIARISTA;
    private String LOGRADOURO_DIARISTA;
    private String NUMERO_DIARISTA;
    private String COMPLEMENTO_DIARISTA;
    private String BAIRRO_DIARISTA;
    private String CIDADE_DIARISTA;
    private String UF_DIARISTA;
    private String LATITUDE_DIARISTA;
    private String LONGITUDE_DIARISTA;
    private String NOMEBENEFICIARIO_DIARISTA;
    private String CONTA_DIARISTA;
    private String AGENCIA_DIARISTA;
    private String EMAIL_DIARISTA;
    private String SENHA_DIARISTA;
    private String STATUS_DIARISTA;
    private Boolean CHECK;

    protected Diarista(Parcel in) {
        setID_DIARISTA(in.readInt());
        setFOTO_DIARISTA(in.readString());
        setNOME_DIARISTA(in.readString());
        setCPF_DIARISTA(in.readString());
        setTELEFONE_DIARISTA(in.readString());
        setCELULAR_DIARISTA(in.readString());
        setDATANASCIMENTO_DIARISTA(in.readString());
        setDESCRICAO_DIARISTA(in.readString());
        setCEP_DIARISTA(in.readString());
        setLOGRADOURO_DIARISTA(in.readString());
        setNUMERO_DIARISTA(in.readString());
        setCOMPLEMENTO_DIARISTA(in.readString());
        setBAIRRO_DIARISTA(in.readString());
        setCIDADE_DIARISTA(in.readString());
        setUF_DIARISTA(in.readString());
        setLATITUDE_DIARISTA(in.readString());
        setLONGITUDE_DIARISTA(in.readString());
        setNOMEBENEFICIARIO_DIARISTA(in.readString());
        setCONTA_DIARISTA(in.readString());
        setAGENCIA_DIARISTA(in.readString());
        setEMAIL_DIARISTA(in.readString());
        setSENHA_DIARISTA(in.readString());
        setSTATUS_DIARISTA(in.readString());
    }

    public static final Creator<Diarista> CREATOR = new Creator<Diarista>() {
        @Override
        public Diarista createFromParcel(Parcel in) {
            return new Diarista(in);
        }

        @Override
        public Diarista[] newArray(int size) {
            return new Diarista[size];
        }
    };

    public int getID_DIARISTA() {
        return ID_DIARISTA;
    }

    public void setID_DIARISTA(int ID_DIARISTA) {
        this.ID_DIARISTA = ID_DIARISTA;
    }

    public String getFOTO_DIARISTA() {
        return FOTO_DIARISTA;
    }

    public void setFOTO_DIARISTA(String FOTO_DIARISTA) {
        this.FOTO_DIARISTA = FOTO_DIARISTA;
    }

    public String getNOME_DIARISTA() {
        return NOME_DIARISTA;
    }

    public void setNOME_DIARISTA(String NOME_DIARISTA) {
        this.NOME_DIARISTA = NOME_DIARISTA;
    }

    public String getCPF_DIARISTA() {
        return CPF_DIARISTA;
    }

    public void setCPF_DIARISTA(String CPF_DIARISTA) {
        this.CPF_DIARISTA = CPF_DIARISTA;
    }

    public String getTELEFONE_DIARISTA() {
        return TELEFONE_DIARISTA;
    }

    public void setTELEFONE_DIARISTA(String TELEFONE_DIARISTA) {
        this.TELEFONE_DIARISTA = TELEFONE_DIARISTA;
    }

    public String getCELULAR_DIARISTA() {
        return CELULAR_DIARISTA;
    }

    public void setCELULAR_DIARISTA(String CELULAR_DIARISTA) {
        this.CELULAR_DIARISTA = CELULAR_DIARISTA;
    }

    public String getDATANASCIMENTO_DIARISTA() {
        return DATANASCIMENTO_DIARISTA;
    }

    public void setDATANASCIMENTO_DIARISTA(String DATANASCIMENTO_DIARISTA) {
        this.DATANASCIMENTO_DIARISTA = DATANASCIMENTO_DIARISTA;
    }

    public String getDESCRICAO_DIARISTA() {
        return DESCRICAO_DIARISTA;
    }

    public void setDESCRICAO_DIARISTA(String DESCRICAO_DIARISTA) {
        this.DESCRICAO_DIARISTA = DESCRICAO_DIARISTA;
    }

    public String getCEP_DIARISTA() {
        return CEP_DIARISTA;
    }

    public void setCEP_DIARISTA(String CEP_DIARISTA) {
        this.CEP_DIARISTA = CEP_DIARISTA;
    }

    public String getLOGRADOURO_DIARISTA() {
        return LOGRADOURO_DIARISTA;
    }

    public void setLOGRADOURO_DIARISTA(String LOGRADOURO_DIARISTA) {
        this.LOGRADOURO_DIARISTA = LOGRADOURO_DIARISTA;
    }

    public String getNUMERO_DIARISTA() {
        return NUMERO_DIARISTA;
    }

    public void setNUMERO_DIARISTA(String NUMERO_DIARISTA) {
        this.NUMERO_DIARISTA = NUMERO_DIARISTA;
    }

    public String getCOMPLEMENTO_DIARISTA() {
        return COMPLEMENTO_DIARISTA;
    }

    public void setCOMPLEMENTO_DIARISTA(String COMPLEMENTO_DIARISTA) {
        this.COMPLEMENTO_DIARISTA = COMPLEMENTO_DIARISTA;
    }

    public String getBAIRRO_DIARISTA() {
        return BAIRRO_DIARISTA;
    }

    public void setBAIRRO_DIARISTA(String BAIRRO_DIARISTA) {
        this.BAIRRO_DIARISTA = BAIRRO_DIARISTA;
    }

    public String getCIDADE_DIARISTA() {
        return CIDADE_DIARISTA;
    }

    public void setCIDADE_DIARISTA(String CIDADE_DIARISTA) {
        this.CIDADE_DIARISTA = CIDADE_DIARISTA;
    }

    public String getUF_DIARISTA() {
        return UF_DIARISTA;
    }

    public void setUF_DIARISTA(String UF_DIARISTA) {
        this.UF_DIARISTA = UF_DIARISTA;
    }

    public String getLATITUDE_DIARISTA() {
        return LATITUDE_DIARISTA;
    }

    public void setLATITUDE_DIARISTA(String LATITUDE_DIARISTA) {
        this.LATITUDE_DIARISTA = LATITUDE_DIARISTA;
    }

    public String getLONGITUDE_DIARISTA() {
        return LONGITUDE_DIARISTA;
    }

    public void setLONGITUDE_DIARISTA(String LONGITUDE_DIARISTA) {
        this.LONGITUDE_DIARISTA = LONGITUDE_DIARISTA;
    }

    public String getNOMEBENEFICIARIO_DIARISTA() {
        return NOMEBENEFICIARIO_DIARISTA;
    }

    public void setNOMEBENEFICIARIO_DIARISTA(String NOMEBENEFICIARIO_DIARISTA) {
        this.NOMEBENEFICIARIO_DIARISTA = NOMEBENEFICIARIO_DIARISTA;
    }

    public String getCONTA_DIARISTA() {
        return CONTA_DIARISTA;
    }

    public void setCONTA_DIARISTA(String CONTA_DIARISTA) {
        this.CONTA_DIARISTA = CONTA_DIARISTA;
    }

    public String getAGENCIA_DIARISTA() {
        return AGENCIA_DIARISTA;
    }

    public void setAGENCIA_DIARISTA(String AGENCIA_DIARISTA) {
        this.AGENCIA_DIARISTA = AGENCIA_DIARISTA;
    }

    public String getEMAIL_DIARISTA() {
        return EMAIL_DIARISTA;
    }

    public void setEMAIL_DIARISTA(String EMAIL_DIARISTA) {
        this.EMAIL_DIARISTA = EMAIL_DIARISTA;
    }

    public String getSENHA_DIARISTA() {
        return SENHA_DIARISTA;
    }

    public void setSENHA_DIARISTA(String SENHA_DIARISTA) {
        this.SENHA_DIARISTA = SENHA_DIARISTA;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(getID_DIARISTA());
        dest.writeString(getFOTO_DIARISTA());
        dest.writeString(getNOME_DIARISTA());
        dest.writeString(getCPF_DIARISTA());
        dest.writeString(getTELEFONE_DIARISTA());
        dest.writeString(getCELULAR_DIARISTA());
        dest.writeString(getDATANASCIMENTO_DIARISTA());
        dest.writeString(getDESCRICAO_DIARISTA());
        dest.writeString(getCEP_DIARISTA());
        dest.writeString(getLOGRADOURO_DIARISTA());
        dest.writeString(getNUMERO_DIARISTA());
        dest.writeString(getCOMPLEMENTO_DIARISTA());
        dest.writeString(getBAIRRO_DIARISTA());
        dest.writeString(getCIDADE_DIARISTA());
        dest.writeString(getUF_DIARISTA());
        dest.writeString(getLATITUDE_DIARISTA());
        dest.writeString(getLONGITUDE_DIARISTA());
        dest.writeString(getNOMEBENEFICIARIO_DIARISTA());
        dest.writeString(getCONTA_DIARISTA());
        dest.writeString(getAGENCIA_DIARISTA());
        dest.writeString(getEMAIL_DIARISTA());
        dest.writeString(getSENHA_DIARISTA());
        dest.writeString(getCONTA_DIARISTA());
    }

    public Boolean getCHECK() {
        return CHECK;
    }

    public void setCHECK(Boolean CHECK) {
        this.CHECK = CHECK;
    }

    public String getSTATUS_DIARISTA() {
        return STATUS_DIARISTA;
    }

    public void setSTATUS_DIARISTA(String STATUS_DIARISTA) {
        this.STATUS_DIARISTA = STATUS_DIARISTA;
    }
}
