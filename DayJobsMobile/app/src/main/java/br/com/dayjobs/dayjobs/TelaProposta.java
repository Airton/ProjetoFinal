package br.com.dayjobs.dayjobs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.dayjobs.dayjobs.Adapter.ServicoAdapter;
import br.com.dayjobs.dayjobs.ClasseBasica.Cliente;
import br.com.dayjobs.dayjobs.ClasseBasica.Servicos;

public class TelaProposta extends AppCompatActivity {
    List<Servicos> listaSelecionada;
    ListView mListViewServicos;
    View view;
    Layout layout;
    LayoutInflater mInflater;
    Activity activity;
    fragmentListaSelecionada fragmentListaSelecionada;
    Cliente cliente;
    ViewPager mViewPager;
    TelaProposta.SelectorPageAdapter mselectorPageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proposta_cliente);

        /*Intent it = getIntent();
        cliente = it.getParcelableExtra("cliente");*/

        buildViewPager();
    }

    public void SolicitarDiarista() {

    }

    public void preencherTelaCliente(){
        View v = (View)findViewById(R.id.relative_lista_selecionada);
        TextView NomeCliente = (TextView) v.findViewById(R.id.txt_nome_cliente);
        TextView NomeDiarista = (TextView) v.findViewById(R.id.txt_nome_diarista);
    }

    private void buildViewPager() {
        mViewPager = (ViewPager) findViewById(R.id.container_teste);
        mselectorPageAdapter = new TelaProposta.SelectorPageAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mselectorPageAdapter);

        TabLayout tab = (TabLayout) findViewById(R.id.tab_teste);
        tab.setupWithViewPager(mViewPager);
    }

    public class SelectorPageAdapter extends FragmentPagerAdapter {
        public SelectorPageAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                default:
                    fragmentListaSelecionada = new fragmentListaSelecionada();
                    return fragmentListaSelecionada;
            }
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public CharSequence getPageTitle(int Position) {
            switch (Position) {
                case 0:
                default:
                    return "TESTE";
            }
        }
    }

}