package br.com.dayjobs.dayjobs;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class ServicosCliente extends AppCompatActivity {
    ViewPager mViewPager;
    SelectorPageAdapter mselectorPageAdapter;
    Toolbar toolbar;
    ServicoListFragment ServicoListFragment;
    DiaServicosListFragment diaServicosListFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.servicos_cliente);
        /*toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        buildViewPager();
    }
    private void buildViewPager() {
        mViewPager = (ViewPager) findViewById(R.id.container_cliente);
        mselectorPageAdapter =  new SelectorPageAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mselectorPageAdapter);

        TabLayout tab = (TabLayout) findViewById(R.id.tabe_cliente);
        tab.setupWithViewPager(mViewPager);
    }

    public class SelectorPageAdapter extends FragmentPagerAdapter {
        public SelectorPageAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    ServicoListFragment =  new ServicoListFragment();
                    return ServicoListFragment;
                case 1:
                    diaServicosListFragment =  new DiaServicosListFragment();
                    return diaServicosListFragment;
                case 2:
                default:
                    diaServicosListFragment =  new DiaServicosListFragment();
                    return diaServicosListFragment;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int Position) {
            switch (Position) {
                case 0:
                    return "Serviços";
                case 1:
                    return "Solicitados";
                case 2:
                default:
                    return "Histórico";
            }
        }
    }
}