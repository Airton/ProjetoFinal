package br.com.dayjobs.dayjobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptor;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.dayjobs.dayjobs.Adapter.DiaristaAdapter;
import br.com.dayjobs.dayjobs.Adapter.DiaristaSelecionadaAdapter;
import br.com.dayjobs.dayjobs.Adapter.ServicoAdapter;
import br.com.dayjobs.dayjobs.ClasseBasica.Diarista;

/**
 * Created by Carlos Eduardo on 30/05/2017.
 */

public class FragmentDiaristaSelecionada extends Fragment {
    Diarista lista, diarista, objeto;
    BitmapDescriptor icon;
    List<Diarista> listaDiarista;
    ListView mListaDiarista;
    SoapObject result, resultDiarista;
    private static String METHOD_NAME = "";
    View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frame_lista_diarista, container, false);
        ReceberLista();
        mListaDiarista = (ListView) view.findViewById(R.id.Lista_Diarista);
        if (listaDiarista.size() > 0) {
            mListaDiarista.setAdapter(new DiaristaSelecionadaAdapter(getActivity(), listaDiarista));
        }
        return view;
    }

    public void ReceberLista() {
        listaDiarista = new ArrayList<>();
        Thread thread = new Thread() {
            @Override
            public void run() {
                String SOAP_ACTION = "http://webservice.dayjobs.com.br/ListarDiarista";
                String NAMESPACE = "http://webservice.dayjobs.com.br/";
                METHOD_NAME = "ListarDiarista";
                String URL = "http://webservice.dayjobs.com.br/WebService.asmx?WSDL";

                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;

                envelope.setOutputSoapObject(request);

                HttpTransportSE transporte = new HttpTransportSE(URL);

                try {
                    transporte.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();

                    for (int cont = 0; result.getPropertyCount() > cont; cont++) {
                        resultDiarista = (SoapObject) result.getProperty(cont);
                        objeto = new Diarista();
                        ObjDiaristaPreencher();
                        listaDiarista.add(objeto);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    public void ObjDiaristaPreencher() {
        try {
            if (resultDiarista.hasProperty("NomeDiarista")) {
                objeto.setID_DIARISTA(Integer.parseInt(resultDiarista.getPropertyAsString("IdDiarista").toString()));
                objeto.setFOTO_DIARISTA(resultDiarista.getPropertySafelyAsString("FotoDiarista").toString());
                objeto.setNOME_DIARISTA(resultDiarista.getPropertyAsString("NomeDiarista").toString());
                objeto.setCPF_DIARISTA(resultDiarista.getPropertySafelyAsString("CpfDiarista").toString());
                objeto.setTELEFONE_DIARISTA(resultDiarista.getPropertySafelyAsString("TelefoneDiarista").toString());
                objeto.setCELULAR_DIARISTA(resultDiarista.getPropertySafelyAsString("CelularDiarista").toString());
                objeto.setDATANASCIMENTO_DIARISTA(resultDiarista.getPropertySafelyAsString("DataNascimentoDiarista").toString());
                objeto.setDESCRICAO_DIARISTA(resultDiarista.getPropertySafelyAsString("DescricaoDiarista").toString());
                objeto.setCEP_DIARISTA(resultDiarista.getPropertySafelyAsString("CepDiarista").toString());
                objeto.setLOGRADOURO_DIARISTA(resultDiarista.getPropertySafelyAsString("LogradouroDiarista").toString());
                objeto.setNUMERO_DIARISTA(resultDiarista.getPropertySafelyAsString("NumeroDiarista").toString());
                objeto.setCOMPLEMENTO_DIARISTA(resultDiarista.getPropertySafelyAsString("ComplementoDiarista").toString());
                objeto.setBAIRRO_DIARISTA(resultDiarista.getPropertySafelyAsString("BairroDiarista").toString());
                objeto.setCIDADE_DIARISTA(resultDiarista.getPropertySafelyAsString("CidadeDiarista").toString());
                objeto.setUF_DIARISTA(resultDiarista.getPropertySafelyAsString("UfDiarista").toString());
                objeto.setLATITUDE_DIARISTA(resultDiarista.getPropertySafelyAsString("LatitudeDiarista").toString());
                objeto.setLONGITUDE_DIARISTA(resultDiarista.getPropertySafelyAsString("LongitudeDiarista").toString());
                objeto.setNOMEBENEFICIARIO_DIARISTA(resultDiarista.getPropertySafelyAsString("NomeBeneficiarioDiarista").toString());
                objeto.setCONTA_DIARISTA(resultDiarista.getPropertySafelyAsString("ContaDiarista").toString());
                objeto.setAGENCIA_DIARISTA(resultDiarista.getPropertySafelyAsString("AgenciaDiarista").toString());
                objeto.setEMAIL_DIARISTA(resultDiarista.getPropertySafelyAsString("EmailDiarista").toString());
                objeto.setSENHA_DIARISTA(resultDiarista.getPropertySafelyAsString("SenhaDiarista").toString());
            }
        } catch (Exception ex) {
            Toast.makeText(getActivity(), "Erro ao tentar preencher a Diarista", Toast.LENGTH_LONG).show();
        }
    }

}
