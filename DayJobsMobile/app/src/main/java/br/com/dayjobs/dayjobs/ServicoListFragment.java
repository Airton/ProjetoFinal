package br.com.dayjobs.dayjobs;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import br.com.dayjobs.dayjobs.Adapter.ServicoAdapter;
import br.com.dayjobs.dayjobs.ClasseBasica.Diarista;
import br.com.dayjobs.dayjobs.ClasseBasica.Servicos;
import br.com.dayjobs.dayjobs.ClasseBasica.StaticListServico;

/**
 * Created by Carlos Eduardo on 19/05/2017.
 */

public class ServicoListFragment extends Fragment {
    Toolbar toolbar;
    Servicos servicos;
    List<Servicos> ListTeste;
    ListView ListaDeServicos;
    List<Servicos> listaServico;
    private static String METHOD_NAME = "";
    SoapObject result, resultServico;
    Servicos objeto;
    static List<Servicos> listaSelecionada;
    CheckBox CHECK_CLIQUE;
    View view,view2;
    Button FazerProposta;
    ListView mListViewServicos;
    List<Servicos> test;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        listaSelecionada = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.frame_lista_servicos, container, false);
        mListViewServicos = (ListView) view.findViewById(R.id.Lista_Servico);

        FazerProposta = (Button) view.findViewById(R.id.btn_fazer_proposta);
        FazerProposta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticListServico lista;
                Servicos teste2 = new Servicos();
                lista = new StaticListServico();
                test = new ArrayList<Servicos>();
                test = lista.getListasSelecionada();
                Intent intent = new Intent(getContext(), TelaProposta.class);
                intent.putParcelableArrayListExtra("listaSelecionada", (ArrayList<? extends Parcelable>) test);
                startActivity(intent);
                //teste2 = test.get(2);
         /*       for (int cont = 0; test.size() > cont; cont++){
                    teste2 = test.get(cont);
                    Toast.makeText(getContext(), "Checbox de " + teste2.getNOME_SERVICO() + " marcado!", Toast.LENGTH_SHORT).show();
                }
*/
            }
        });
        ReceberLista();
        // servicos = new  Servicos("Teste");
        ListaDeServicos = (ListView) view.findViewById(R.id.Lista_Servico);
        if (listaServico != null) {
            //ListaDeServicos.setAdapter(new DiaristaAdapter(getActivity(), listaTest));
            ListaDeServicos.setAdapter(new ServicoAdapter(getActivity(), listaServico));
        }
        return view;
    }

    public void ReceberLista() {
        listaServico = new ArrayList<>();
        Thread thread = new Thread() {
            @Override
            public void run() {
                String SOAP_ACTION = "http://webservice.dayjobs.com.br/ListarServico";
                String NAMESPACE = "http://webservice.dayjobs.com.br/";
                METHOD_NAME = "ListarServico";
                String URL = "http://webservice.dayjobs.com.br/WebService.asmx?WSDL";

                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;

                envelope.setOutputSoapObject(request);

                HttpTransportSE transporte = new HttpTransportSE(URL);

                try {
                    transporte.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();

                    for (int cont = 0; result.getPropertyCount() > cont; cont++) {
                        resultServico = (SoapObject) result.getProperty(cont);
                        objeto = new Servicos();
                        ObjServicoPreencher();
                        listaServico.add(objeto);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    public void ObjServicoPreencher() {
        try {
            if (resultServico.hasProperty("NomeServico")) {
                objeto.setID_SERVICO(Integer.parseInt(resultServico.getPropertyAsString("IdServico").toString()));
                objeto.setDESCRICAO_SERVICO(resultServico.getPropertySafelyAsString("DescricaoServico").toString());
                objeto.setNOME_SERVICO(resultServico.getPropertySafelyAsString("NomeServico").toString());
            }
        } catch (Exception ex) {
            Toast.makeText(getActivity(), "Erro ao tentar preencher a Serviços", Toast.LENGTH_LONG).show();
        }
    }

}