package br.com.dayjobs.dayjobs.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import br.com.dayjobs.dayjobs.ClasseBasica.Diarista;
import br.com.dayjobs.dayjobs.ClasseBasica.Proposta;
import br.com.dayjobs.dayjobs.ClasseBasica.Servicos;
import br.com.dayjobs.dayjobs.ClasseBasica.StaticListServico;
import br.com.dayjobs.dayjobs.R;
import br.com.dayjobs.dayjobs.ServicoListFragment;
import br.com.dayjobs.dayjobs.TelaProposta;

/**
 * Created by Carlos Eduardo on 19/05/2017.
 */

public class ServicoAdapter extends ArrayAdapter<Servicos> {

    public String linkDaImagem = "https://image.tmdb.org/t/p/w500/";
    View view;
    List<Servicos> ListServico, listaSelecionada;
    StaticListServico staticListServico;
    int checkqtd = 0;
    Servicos servicos;

    public ServicoAdapter(Context context, List<Servicos> servicos) {
        super(context, 0, servicos);
        ListServico = servicos;
    }

    CheckBox mCHECK_CLIQUE;
    ViewHolder holder;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        servicos = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.item_servico, parent, false);
        }

        holder = new ViewHolder();
        holder.NOME_SERVICO = (TextView) convertView.findViewById(R.id.item_nomeServico);
        holder.DESCRICAO_SERVICO = (TextView) convertView.findViewById(R.id.item_descricaoServico);
        holder.CHECK_CLIQUE = (CheckBox) convertView.findViewById(R.id.item_check);
        holder.CHECK_CLIQUE.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    ListServico.get(position).setCHECK(true);
                    onCheckboxClicked(ListServico.get(position).getID_SERVICO());
                    checkqtd = checkqtd + 1;

                } else {
                    ListServico.get(position).setCHECK(false);
                    checkqtd = checkqtd - 1;
                    onCheckboxClickedFalse(ListServico.get(position).getID_SERVICO());
                    //listaSelecionada.remove(checkqtd);
                    staticListServico = new StaticListServico(listaSelecionada);
                }
            }
        });

        if (servicos.isCHECK() == true){
            holder.CHECK_CLIQUE.setChecked(true);
        }
        if (servicos.isCHECK() == false){
            holder.CHECK_CLIQUE.setChecked(false);
        }
        holder.NOME_SERVICO.setText(servicos.getNOME_SERVICO());
        holder.DESCRICAO_SERVICO.setText(servicos.getDESCRICAO_SERVICO());
        return convertView;
    }

    public void onCheckboxClicked(int item) {
        // Check which checkbox was clicked
        Servicos objeto = new Servicos();
        objeto = ListServico.get(item-1);
        if (listaSelecionada == null) {
            listaSelecionada = new ArrayList<>();
        }
        listaSelecionada.add(objeto);
        Intent intent = new Intent(getContext(), TelaProposta.class);
        intent.putParcelableArrayListExtra("listaServico", (ArrayList<? extends Parcelable>) listaSelecionada);
        staticListServico = new StaticListServico(listaSelecionada);
    }

    public void onCheckboxClickedFalse(int item){
        Servicos objeto = new Servicos();
        objeto = ListServico.get(item-1);
        listaSelecionada.remove(objeto);
        staticListServico = new StaticListServico(listaSelecionada);
    }

    static class ViewHolder {
        TextView NOME_SERVICO;
        TextView DESCRICAO_SERVICO;
        CheckBox CHECK_CLIQUE;
    }
}