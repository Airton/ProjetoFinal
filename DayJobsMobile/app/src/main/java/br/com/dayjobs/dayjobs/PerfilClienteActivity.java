package br.com.dayjobs.dayjobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import br.com.dayjobs.dayjobs.ClasseBasica.Cliente;

/**
 * Created by P on 20/04/2017.
 */

/**
 * Created by Brendo Santiago on 23/05/2017.
 */

public class PerfilClienteActivity extends AppCompatActivity {

    protected DrawerLayout drawerLayout;
    Cliente cliente;
    Boolean result;
    private static String METHOD_NAME = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_cliente);

        Intent it = getIntent();
        cliente = it.getParcelableExtra("cliente");

        PreencherTelaCliente();
    }

    public void PreencherTelaCliente() {
        ImageView imageView = (ImageView) findViewById(R.id.img_perfil_cliente);
        EditText input_nomeCliente = (EditText) findViewById(R.id.edt_nome_cliente);
        EditText input_celularCliente = (EditText) findViewById(R.id.edt_celular_cliente);
        EditText input_dataNascimentoCliente = (EditText) findViewById(R.id.edt_data_nascimento_cliente);
        EditText input_cepCliente = (EditText) findViewById(R.id.edt_cep_cliente);
        EditText input_logradouroCliente = (EditText) findViewById(R.id.edt_logradouro_cliente);
        EditText input_numeroCliente = (EditText) findViewById(R.id.edt_numero_cliente);
        EditText input_complementoCliente = (EditText) findViewById(R.id.edt_complemento_cliente);
        EditText input_bairroCliente = (EditText) findViewById(R.id.edt_bairro_cliente);
        EditText input_cidadeCliente = (EditText) findViewById(R.id.edt_cidade_cliente);
        EditText input_ufCliente = (EditText) findViewById(R.id.edt_uf_cliente);
        EditText input_email = (EditText) findViewById(R.id.edt_email_cliente);

        Glide.with(this).load(cliente.getFOTO_CLIENTE()).into(imageView);
        input_nomeCliente.setText(cliente.getNOME_CLIENTE());
        input_celularCliente.setText(cliente.getCELULAR_CLIENTE());
        if (cliente.getDATANASCIMENTO_CLIENTE().equals("anyType{}")) {
            input_dataNascimentoCliente.setText("");
        } else {
            input_dataNascimentoCliente.setText(cliente.getDATANASCIMENTO_CLIENTE());
        }
        if (cliente.getCEP_CLIENTE().equals("anyType{}")) {
            input_cepCliente.setText("");
        } else {
            input_cepCliente.setText(cliente.getCEP_CLIENTE());
        }
        if (cliente.getLOGRADOURO_CLIENTE().equals("anyType{}")) {
            input_logradouroCliente.setText("");
        } else {
            input_logradouroCliente.setText(cliente.getLOGRADOURO_CLIENTE());
        }
        if (cliente.getNUMERO_CLIENTE().equals("anyType{}")) {
            input_numeroCliente.setText("");
        } else {
            input_numeroCliente.setText(cliente.getNUMERO_CLIENTE());
        }
        if (cliente.getCOMPLEMENTO_CLIENTE().equals("anyType{}")) {
            input_complementoCliente.setText("");
        } else {
            input_complementoCliente.setText(cliente.getCOMPLEMENTO_CLIENTE());
        }
        if (cliente.getBAIRRO_CLIENTE().equals("anyType{}")) {
            input_bairroCliente.setText("");
        } else {
            input_bairroCliente.setText(cliente.getCOMPLEMENTO_CLIENTE());
        }
        if (cliente.getCIDADE_CLIENTE().equals("anyType{}")) {
            input_cidadeCliente.setText("");
        } else {
            input_cidadeCliente.setText(cliente.getCIDADE_CLIENTE());
        }
        if (cliente.getUF_CLIENTE().equals("anyType{}")) {
            input_ufCliente.setText("");
        } else {
            input_ufCliente.setText(cliente.getUF_CLIENTE());
        }
        input_email.setText(cliente.getEMAIL_CLIENTE());
    }

//    public void RetornoCliente() {
//        TextView input_nomeCliente = (TextView) findViewById(R.id.input_nomeCliente);
//        TextView input_celularCliente = (TextView) findViewById(R.id.input_celularCliente);
//        TextView input_cepCliente = (TextView) findViewById(R.id.input_cepCliente);
//        TextView input_logradouroCliente = (TextView) findViewById(R.id.input_logradouroCliente);
//        TextView input_numeroCliente = (TextView) findViewById(R.id.input_numeroCliente);
//        TextView input_bairroCliente = (TextView) findViewById(R.id.input_bairroCliente);
//        TextView input_cidadeCliente = (TextView) findViewById(R.id.input_cidadeCliente);
//        TextView input_ufCliente = (TextView) findViewById(R.id.input_ufCliente);
//        TextView input_email = (TextView) findViewById(R.id.input_email);
//        TextView input_password = (TextView) findViewById(R.id.input_password);
//
//        cliente.setNOME_CLIENTE(input_nomeCliente.getText().toString());
//        cliente.setCELULAR_CLIENTE(input_celularCliente.getText().toString());
//        cliente.setCEP_CLIENTE(input_cepCliente.getText().toString());
//        cliente.setLOGRADOURO_CLIENTE(input_logradouroCliente.getText().toString());
//        cliente.setNUMERO_CLIENTE(input_numeroCliente.getText().toString());
//        cliente.setBAIRRO_CLIENTE(input_bairroCliente.getText().toString());
//        cliente.setCIDADE_CLIENTE(input_cidadeCliente.getText().toString());
//        cliente.setUF_CLIENTE(input_ufCliente.getText().toString());
//        cliente.setEMAIL_CLIENTE(input_email.getText().toString());
//        cliente.setSENHA_CLIENTE(input_password.getText().toString());
//    }

//    public void EnviarOnClick(View v) {
//        RetornoCliente();
//
//        Thread nt = new Thread() {
//            @Override
//            public void run() {
//                String SOAP_ACTION = "http://webservice.dayjobs.com.br/AlterarCliente";
//                String NAMESPACE = "http://webservice.dayjobs.com.br/";
//                METHOD_NAME = "AlterarCliente";
//                String URL = "http://webservice.dayjobs.com.br/WebService.asmx?WSDL";
//
//                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
//                // requisição passando o parametro do metodo e o valor a colocar.
//                request.addProperty("cliente", cliente);
//                request.addProperty("id", cliente.getID_CLIENTE());
//
//                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//                envelope.dotNet = true;
//
//                envelope.setOutputSoapObject(request);
//
//                HttpTransportSE transporte = new HttpTransportSE(URL);
//
//                try {
//                    transporte.call(SOAP_ACTION, envelope);
//                    //SoapPrimitive resultado = (SoapPrimitive) envelope.getResponse();
//                    result = (Boolean) envelope.getResponse();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (XmlPullParserException e) {
//                    e.printStackTrace();
//                }
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (result.equals(true)) {
//                            Toast.makeText(PerfilClienteActivity.this, "Alteração realizada com sucesso!", Toast.LENGTH_LONG).show();
//                        } else {
//                            Toast.makeText(PerfilClienteActivity.this, "Erro ao alterar.", Toast.LENGTH_LONG).show();
//                        }
//                    }
//                });
//            }
//        };
//        nt.start();
//    }
}