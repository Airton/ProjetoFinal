package br.com.dayjobs.dayjobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import br.com.dayjobs.dayjobs.ClasseBasica.Cliente;

/**
 * Updated by Brendo Santiago on 21/05/2017.
 */

public class TelaPrincipalCliente extends livroandroid.lib.activity.BaseActivity {

    protected DrawerLayout drawerLayout;
    Cliente cliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_principal_cliente);

        Intent it = getIntent();
        cliente = it.getParcelableExtra("cliente");

        Toast.makeText(this, "Seja bem-vindo, " + cliente.getNOME_CLIENTE() + "!", Toast.LENGTH_LONG).show();

        setUpToolbar();
        setupNavDrawer();

        MapsFragment mapsFragment = new MapsFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.fragment_container_cliente, mapsFragment, "MapsFragment");
        transaction.commitAllowingStateLoss();
    }

    //Botão BACK padrão do android
    @Override
    public void onBackPressed() {
        //O efeito ao ser pressionado do botão (no caso abre a activity)
        startActivity(new Intent(this, LoginClienteActivity.class));
        //Método para matar a activity e não deixa-lá indexada na pilhagem
        finish();
        return;
    }

    //Métodos da BaseActivity -> Início
    private FragmentManager baseFragmentManager;

    //Configura a Toolbar
    protected void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_cliente);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    //Configura o nav drawer
    protected void setupNavDrawer() {
        //Drawer Layout
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            //Ícone do menu do Nav Drawer
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setDisplayHomeAsUpEnabled(true);
            drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_cliente);
            if (navigationView != null && drawerLayout != null) {
                //Atualizar a imagem e os textos do header do menu lateral
                setNavViewValues(navigationView, cliente.getNOME_CLIENTE(), cliente.getEMAIL_CLIENTE(), cliente.getFOTO_CLIENTE());
                //Trata o evento de clique no menu
                navigationView.setNavigationItemSelectedListener(
                        new NavigationView.OnNavigationItemSelectedListener() {
                            @Override
                            public boolean onNavigationItemSelected(MenuItem menuItem) {
                                // Seleciona a linha
                                menuItem.setChecked(true);
                                // Fecha o menu
                                drawerLayout.closeDrawers();
                                // Trata o evento do menu
                                onNavDrawerItemSelected(menuItem);
                                return true;
                            }
                        });
            }
        }
    }

    //Atualiza os dados do header do Navigation View
    private void setNavViewValues(NavigationView navView, String nome, String email, String foto) {
        View headerView = navView.getHeaderView(0);
        TextView tNome = (TextView) headerView.findViewById(R.id.nome_perfil);
        TextView tEmail = (TextView) headerView.findViewById(R.id.email_perfil);
        ImageView imgView = (ImageView) headerView.findViewById(R.id.img_perfil);
        tNome.setText(nome);
        tEmail.setText(email);
        Glide.with(this).load(foto).into(imgView);
    }

    //Trata o evento do menu lateral
    private void onNavDrawerItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_item_principal:
                TelaPrincipalCliente.this.onStart();
                break;
            case R.id.nav_item_perfil:
                Intent intent1 = new Intent(this, PerfilClienteActivity.class);
                intent1.putExtra("cliente", cliente);
                startActivity(intent1);
                break;
            case R.id.nav_item_servico:
                Intent intent2 = new Intent(this, ServicosCliente.class);
                intent2.putExtra("cliente", cliente);
                startActivity(intent2);
                break;
            case R.id.nav_item_sair:
                Intent intent3 = new Intent(getApplicationContext(), LoginClienteActivity.class);
                //intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //intent3.putExtra("SAIR", true);
                startActivity(intent3);
                getActivity().finish();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Trata o clique no botão que abre o menu
                if (drawerLayout != null) {
                    openDrawer();
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    // Abre o menu lateral
    protected void openDrawer() {
        if (drawerLayout != null) {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    // Fecha o menu lateral
    protected void closeDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }
    //Métodos da BaseActivity -> Fim
}