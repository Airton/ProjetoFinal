package br.com.dayjobs.dayjobs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.List;

import br.com.dayjobs.dayjobs.Adapter.ServicoAdapter;
import br.com.dayjobs.dayjobs.ClasseBasica.Servicos;
import br.com.dayjobs.dayjobs.ClasseBasica.StaticListServico;

/**
 * Created by Carlos Eduardo on 30/05/2017.
 */

public class fragmentListaSelecionada extends Fragment {
    Toolbar toolbar;
    Servicos servicos;
    List<Servicos> ListTeste;
    ListView ListaDeServicos;
    List<Servicos> listaServico;
    private static String METHOD_NAME = "";
    SoapObject result, resultServico;
    Servicos objeto;
    static List<Servicos> listaRecebida;
    CheckBox CHECK_CLIQUE;
    View view,view2;
    Button FazerProposta;
    ListView mListViewServicos;
    List<Servicos> test;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        listaRecebida = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_lista_selecionada, container, false);

        StaticListServico lista;
        Servicos teste2 = new Servicos();
        lista = new StaticListServico();
        test = new ArrayList<Servicos>();
        test = lista.getListasSelecionada();

        mListViewServicos = (ListView) view.findViewById(R.id.listView);
        mListViewServicos.setAdapter(new ServicoAdapter(getActivity(), test));

        return view;
    }

}