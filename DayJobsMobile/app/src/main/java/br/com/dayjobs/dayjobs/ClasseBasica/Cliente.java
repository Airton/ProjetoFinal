package br.com.dayjobs.dayjobs.ClasseBasica;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by P on 19/04/2017.
 */

public class Cliente implements Parcelable {

    public static final Creator<Cliente> CREATOR = new Creator<Cliente>() {
        @Override
        public Cliente createFromParcel(Parcel in) {
            return new Cliente(in);
        }

        @Override
        public Cliente[] newArray(int size) {
            return new Cliente[size];
        }
    };

    private int ID_CLIENTE;
    private String FOTO_CLIENTE;
    private String NOME_CLIENTE;
    private String CELULAR_CLIENTE;
    private String DATANASCIMENTO_CLIENTE;
    private String CEP_CLIENTE;
    private String LOGRADOURO_CLIENTE;
    private String NUMERO_CLIENTE;
    private String COMPLEMENTO_CLIENTE;
    private String BAIRRO_CLIENTE;
    private String CIDADE_CLIENTE;
    private String UF_CLIENTE;
    private String LATITUDE_CLIENTE;
    private String LONGITUDE_CLIENTE;
    private String EMAIL_CLIENTE;
    private String SENHA_CLIENTE;

    public Cliente() {

    }

    protected Cliente(Parcel in) {
        ID_CLIENTE = in.readInt();
        FOTO_CLIENTE = in.readString();
        NOME_CLIENTE = in.readString();
        CELULAR_CLIENTE = in.readString();
        DATANASCIMENTO_CLIENTE = in.readString();
        CEP_CLIENTE = in.readString();
        LOGRADOURO_CLIENTE = in.readString();
        NUMERO_CLIENTE = in.readString();
        COMPLEMENTO_CLIENTE = in.readString();
        BAIRRO_CLIENTE = in.readString();
        CIDADE_CLIENTE = in.readString();
        UF_CLIENTE = in.readString();
        LATITUDE_CLIENTE = in.readString();
        LONGITUDE_CLIENTE = in.readString();
        EMAIL_CLIENTE = in.readString();
        SENHA_CLIENTE = in.readString();
    }

    public int getID_CLIENTE() {
        return ID_CLIENTE;
    }

    public void setID_CLIENTE(int ID_CLIENTE) {
        this.ID_CLIENTE = ID_CLIENTE;
    }

    public String getFOTO_CLIENTE() {
        return FOTO_CLIENTE;
    }

    public void setFOTO_CLIENTE(String FOTO_CLIENTE) {
        this.FOTO_CLIENTE = FOTO_CLIENTE;
    }

    public String getNOME_CLIENTE() {
        /*if (NOME_CLIENTE.contains("anyType{}")) {
            return NOME_CLIENTE = "";
        }*/
        return NOME_CLIENTE;
    }

    public void setNOME_CLIENTE(String NOME_CLIENTE) {
        this.NOME_CLIENTE = NOME_CLIENTE;
    }

    public String getCELULAR_CLIENTE() {
       /* if (CELULAR_CLIENTE.contains("anyType{}")) {
            return CELULAR_CLIENTE = "";
        }*/
        return CELULAR_CLIENTE;
    }

    public void setCELULAR_CLIENTE(String CELULAR_CLIENTE) {
        this.CELULAR_CLIENTE = CELULAR_CLIENTE;
    }

    public String getDATANASCIMENTO_CLIENTE() {
      /*  if (DATANASCIMENTO_CLIENTE.contains("anyType{}")) {
            return DATANASCIMENTO_CLIENTE = "";
        }*/
        return DATANASCIMENTO_CLIENTE;
    }

    public void setDATANASCIMENTO_CLIENTE(String DATANASCIMENTO_CLIENTE) {
        this.DATANASCIMENTO_CLIENTE = DATANASCIMENTO_CLIENTE;
    }

    public String getCEP_CLIENTE() {
      /*  if (CEP_CLIENTE.contains("anyType{}")) {
            return CEP_CLIENTE = "";
        }*/
        return CEP_CLIENTE;
    }

    public void setCEP_CLIENTE(String CEP_CLIENTE) {
        this.CEP_CLIENTE = CEP_CLIENTE;
    }

    public String getLOGRADOURO_CLIENTE() {
        /*if (LOGRADOURO_CLIENTE.contains("anyType{}")) {
            return LOGRADOURO_CLIENTE = "";
        }*/
        return LOGRADOURO_CLIENTE;
    }

    public void setLOGRADOURO_CLIENTE(String LOGRADOURO_CLIENTE) {
        this.LOGRADOURO_CLIENTE = LOGRADOURO_CLIENTE;
    }

    public String getNUMERO_CLIENTE() {
       /* if (NUMERO_CLIENTE.contains("anyType{}")) {
            return NUMERO_CLIENTE = "";
        }*/
        return NUMERO_CLIENTE;
    }

    public void setNUMERO_CLIENTE(String NUMERO_CLIENTE) {
        this.NUMERO_CLIENTE = NUMERO_CLIENTE;
    }

    public String getCOMPLEMENTO_CLIENTE() {
       /* if (COMPLEMENTO_CLIENTE.contains("anyType{}")) {
            return COMPLEMENTO_CLIENTE = "";
        }*/
        return COMPLEMENTO_CLIENTE;
    }

    public void setCOMPLEMENTO_CLIENTE(String COMPLEMENTO_CLIENTE) {
        this.COMPLEMENTO_CLIENTE = COMPLEMENTO_CLIENTE;
    }

    public String getBAIRRO_CLIENTE() {
      /*  if (BAIRRO_CLIENTE.contains("anyType{}")) {
            return BAIRRO_CLIENTE = "";
        }*/
        return BAIRRO_CLIENTE;
    }

    public void setBAIRRO_CLIENTE(String BAIRRO_CLIENTE) {
        this.BAIRRO_CLIENTE = BAIRRO_CLIENTE;
    }

    public String getCIDADE_CLIENTE() {
       /* if (CIDADE_CLIENTE.contains("anyType{}")) {
            return CIDADE_CLIENTE = "";
        }*/
        return CIDADE_CLIENTE;
    }

    public void setCIDADE_CLIENTE(String CIDADE_CLIENTE) {
        this.CIDADE_CLIENTE = CIDADE_CLIENTE;
    }

    public String getUF_CLIENTE() {
      /*  if (UF_CLIENTE.contains("anyType{}")) {
            return UF_CLIENTE = "";
        }*/
        return UF_CLIENTE;
    }

    public void setUF_CLIENTE(String UF_CLIENTE) {
        this.UF_CLIENTE = UF_CLIENTE;
    }

    public String getEMAIL_CLIENTE() {
        /*if (EMAIL_CLIENTE.contains("anyType{}")) {
            return EMAIL_CLIENTE = "";
        }*/
        return EMAIL_CLIENTE;
    }

    public void setEMAIL_CLIENTE(String EMAIL_CLIENTE) {
        this.EMAIL_CLIENTE = EMAIL_CLIENTE;
    }

    public String getSENHA_CLIENTE() {
      /*  if (SENHA_CLIENTE.contains("anyType{}")) {
            return SENHA_CLIENTE = "";
        }*/
        return SENHA_CLIENTE;
    }

    public void setSENHA_CLIENTE(String SENHA_CLIENTE) {
        this.SENHA_CLIENTE = SENHA_CLIENTE;
    }

    public String getLATITUDE_CLIENTE() {
        return LATITUDE_CLIENTE;
    }

    public void setLATITUDE_CLIENTE(String LATITUDE_CLIENTE) {
        this.LATITUDE_CLIENTE = LATITUDE_CLIENTE;
    }

    public String getLONGITUDE_CLIENTE() {
        return LONGITUDE_CLIENTE;
    }

    public void setLONGITUDE_CLIENTE(String LONGITUDE_CLIENTE) {
        this.LONGITUDE_CLIENTE = LONGITUDE_CLIENTE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID_CLIENTE);
        dest.writeString(FOTO_CLIENTE);
        dest.writeString(NOME_CLIENTE);
        dest.writeString(CELULAR_CLIENTE);
        dest.writeString(DATANASCIMENTO_CLIENTE);
        dest.writeString(CEP_CLIENTE);
        dest.writeString(LOGRADOURO_CLIENTE);
        dest.writeString(NUMERO_CLIENTE);
        dest.writeString(COMPLEMENTO_CLIENTE);
        dest.writeString(BAIRRO_CLIENTE);
        dest.writeString(CIDADE_CLIENTE);
        dest.writeString(UF_CLIENTE);
        dest.writeString(LATITUDE_CLIENTE);
        dest.writeString(LONGITUDE_CLIENTE);
        dest.writeString(EMAIL_CLIENTE);
        dest.writeString(SENHA_CLIENTE);
    }
}